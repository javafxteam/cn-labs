﻿namespace SeeSharpImplementation
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.precisionLabel = new System.Windows.Forms.Label();
            this.computeBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._clearItemsBtn = new System.Windows.Forms.Button();
            this._loadItemsBTN = new System.Windows.Forms.Button();
            this._loadedItemsTB = new System.Windows.Forms.ListBox();
            this.precisionUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._workProgress = new System.Windows.Forms.ProgressBar();
            this.logTB = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.precisionUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // precisionLabel
            // 
            this.precisionLabel.AutoSize = true;
            this.precisionLabel.Location = new System.Drawing.Point(6, 27);
            this.precisionLabel.Name = "precisionLabel";
            this.precisionLabel.Size = new System.Drawing.Size(128, 15);
            this.precisionLabel.TabIndex = 0;
            this.precisionLabel.Text = "Precision (sys max: 16):";
            // 
            // computeBtn
            // 
            this.computeBtn.Location = new System.Drawing.Point(18, 376);
            this.computeBtn.Name = "computeBtn";
            this.computeBtn.Size = new System.Drawing.Size(302, 31);
            this.computeBtn.TabIndex = 1;
            this.computeBtn.Text = "Compute";
            this.computeBtn.UseVisualStyleBackColor = true;
            this.computeBtn.Click += new System.EventHandler(this.computeBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._clearItemsBtn);
            this.groupBox1.Controls.Add(this._loadItemsBTN);
            this.groupBox1.Controls.Add(this._loadedItemsTB);
            this.groupBox1.Controls.Add(this.precisionUpDown);
            this.groupBox1.Controls.Add(this.precisionLabel);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(314, 361);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input Data";
            // 
            // _clearItemsBtn
            // 
            this._clearItemsBtn.Location = new System.Drawing.Point(215, 79);
            this._clearItemsBtn.Name = "_clearItemsBtn";
            this._clearItemsBtn.Size = new System.Drawing.Size(93, 31);
            this._clearItemsBtn.TabIndex = 11;
            this._clearItemsBtn.Text = "Clear";
            this._clearItemsBtn.UseVisualStyleBackColor = true;
            this._clearItemsBtn.Click += new System.EventHandler(this._clearItemsBtn_Click);
            // 
            // _loadItemsBTN
            // 
            this._loadItemsBTN.Location = new System.Drawing.Point(5, 79);
            this._loadItemsBTN.Name = "_loadItemsBTN";
            this._loadItemsBTN.Size = new System.Drawing.Size(207, 31);
            this._loadItemsBTN.TabIndex = 5;
            this._loadItemsBTN.Text = "Add File(s)";
            this._loadItemsBTN.UseVisualStyleBackColor = true;
            this._loadItemsBTN.Click += new System.EventHandler(this._loadItemsBTN_Click);
            // 
            // _loadedItemsTB
            // 
            this._loadedItemsTB.FormattingEnabled = true;
            this._loadedItemsTB.ItemHeight = 15;
            this._loadedItemsTB.Location = new System.Drawing.Point(6, 111);
            this._loadedItemsTB.Name = "_loadedItemsTB";
            this._loadedItemsTB.Size = new System.Drawing.Size(302, 244);
            this._loadedItemsTB.TabIndex = 10;
            // 
            // precisionUpDown
            // 
            this.precisionUpDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.precisionUpDown.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.precisionUpDown.Location = new System.Drawing.Point(6, 50);
            this.precisionUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.precisionUpDown.Name = "precisionUpDown";
            this.precisionUpDown.Size = new System.Drawing.Size(128, 23);
            this.precisionUpDown.TabIndex = 3;
            this.precisionUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.precisionUpDown.ValueChanged += new System.EventHandler(this.precisionUpDown_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._workProgress);
            this.groupBox2.Controls.Add(this.logTB);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(332, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(473, 395);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output Data";
            // 
            // _workProgress
            // 
            this._workProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._workProgress.Location = new System.Drawing.Point(6, 366);
            this._workProgress.Name = "_workProgress";
            this._workProgress.Size = new System.Drawing.Size(461, 23);
            this._workProgress.TabIndex = 1;
            // 
            // logTB
            // 
            this.logTB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logTB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logTB.Location = new System.Drawing.Point(6, 26);
            this.logTB.Name = "logTB";
            this.logTB.Size = new System.Drawing.Size(461, 335);
            this.logTB.TabIndex = 0;
            this.logTB.Text = "";
            this.logTB.WordWrap = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(819, 421);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.computeBtn);
            this.MinimumSize = new System.Drawing.Size(835, 459);
            this.Name = "MainWindow";
            this.Text = "Matrixes Application";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.precisionUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label precisionLabel;
        private System.Windows.Forms.Button computeBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown precisionUpDown;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox logTB;
        private System.Windows.Forms.ListBox _loadedItemsTB;
        private System.Windows.Forms.Button _loadItemsBTN;
        private System.Windows.Forms.Button _clearItemsBtn;
        private System.Windows.Forms.ProgressBar _workProgress;
    }
}

