﻿
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public interface ISolver
    {
        int N { get; set; }
        int Steps { get; set; }
        SpecialMatrix A { get; set; }
        Vector<double> B { get; set; }

        Vector<double> Solve();
    }
}
