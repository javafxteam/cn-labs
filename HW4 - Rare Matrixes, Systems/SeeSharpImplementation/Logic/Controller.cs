﻿using System.IO;
using System.Runtime.InteropServices;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class Controller
    {
        private Backgrounder worker;//represents the worker that runs me. ref it to make him update

        public int N { get; set; }
        public SpecialMatrix A { get; set; }
        public Vector<double> B { get; set; }

        private Solution _solution;

        public Controller(string filePath, Backgrounder worker)
        {
            this.worker = worker;

            using (var reader = new StreamReader(filePath))
            {
                // read N
                this.worker.AddLogMessage("Reading data from files...");
                this.worker.UpdateUpperBounds(2);

                N = int.Parse(reader.ReadLine());
                reader.ReadLine();
                worker.UpdateProgress(0);

                // read B
                var bString = reader.ReadMaxNLines(N);
                B = new Vector<double>(N);
                B.BuildFromString(bString);
                worker.UpdateProgress(1);

                // read matrix
                var mString = reader.ReadToEnd();
                worker.UpdateProgress(2);
                A = new SpecialMatrix(N, mString, worker);
            }
        }

        public string GetSolution()
        {
            var gaussSolver = new GsSolver(N, A, B);
            _solution = new Solution(gaussSolver);
            return _solution.ToString();
        }

    }
}
