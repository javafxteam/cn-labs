﻿using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SeeSharpImplementation.Logic
{
    public class Backgrounder
    {
        private BackgroundWorker _bw;
        private ProgressBar _pb;
        private RichTextBox _tb;
        DateTime startTime, endTime;

        public Backgrounder(ProgressBar toUpdate, RichTextBox toLog)
        {
            _pb = toUpdate;
            _tb = toLog;

            _bw = new BackgroundWorker
            {
                WorkerReportsProgress = true
            };

            _bw.ProgressChanged += (o, args) =>
            {
                _pb.SetProgress(args.ProgressPercentage);
                
            };

        }

        public void AddLogMessage(string message, Color? color = null)
        {
            _tb.AppendText(message + "\n", color ?? Color.Gray);
        }

        public void UpdateUpperBounds(int newUpperBound)
        {
            _pb.SetMaxValue(newUpperBound);
        }

        public void UpdateProgress(int percent)
        {
            _bw.ReportProgress(percent);
        }

        public void SetDoWork(Action<object, DoWorkEventArgs> actionToExecute)
        {
            _bw.DoWork += (o, args) =>
            {
                try
                {
                    actionToExecute(o, args);
                }
                catch (Exception exception)
                {
                    _tb.ShowWarningFromException(exception);
                    AddLogMessage($"Operation could not complete. Reason: {exception.Message}.", Color.Red);
                    _pb.SetProgress(0);
                    throw new Exception(exception.Message);
                }
            };
        }

        public void SetOnWorkerCompleted(Action<object, RunWorkerCompletedEventArgs> onActionEnd)
        {
            _bw.RunWorkerCompleted += (o, args) =>
            {
                if (args.Error != null)
                    return;

                onActionEnd(o, args);

                endTime = DateTime.Now;
                var how = (endTime - startTime);
                string duration = how.ToString();
                AddLogMessage($"Operation took {duration}.");
                _pb.SetProgress(0);
            };
        }
        
        public void Start()
        {
            if (_bw == null)
                throw new Exception("Trying to run inexistent action in background.");

            if (_bw.IsBusy)
                throw new Exception("Please first wait for the background operation to end then start a new one.");

            startTime = DateTime.Now;
            _bw.RunWorkerAsync();
        }
    }
}
