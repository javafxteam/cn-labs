﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeSharpImplementation.Logic
{
    internal class Utils
    {
        public static double Precision;
        public static int PrecisionPower;
        public static int MaxStep = 10000;
        public static double MaxValueToCallItInfinite = Math.Pow(10, 15);

        /// <summary>
        /// Checks if a number if zero given the error-precision to take into account.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="precision"></param>
        public static void CheckIfZero(double number, double precision)
        {
            if (Math.Abs(number) <= precision)
                throw new Exception("That number is so close to Zero, Oh Dear...");
        }

        public static double GetNormForDifference(IEnumerable<double> X1, IEnumerable<double> X2)
        {
            var index = 0;
            var difference = X1.Select(x1 => (x1 - X2.ElementAt(index++))).ToList();
            return GetNorm(difference);
        }

        public static double GetNorm(IEnumerable<double> Z)
        {
            var sum = 0.0;
            Z.ToList().ForEach(x => sum += Math.Pow(x, 2));
            return Math.Sqrt(sum);
        }

        public static double GetInfinityNorm(IEnumerable<double> Z)
        {
            return Z.Select(Math.Abs).Max();
        }

        /// <summary>
        /// Given the precision e = 1 / 10^t => Returns t.
        /// </summary>
        /// <returns></returns>
        public static void ComputeSystemPrecisionPower()
        {
            PrecisionPower = 1;
            Precision = 1d / 10;
            const double constant = 1.0;

            while (Math.Abs(constant + Precision - constant) > 0)
            {
                PrecisionPower++;
                Precision /= 10;
            }
            PrecisionPower--;
            Precision *= 10;
        }
    }
}
