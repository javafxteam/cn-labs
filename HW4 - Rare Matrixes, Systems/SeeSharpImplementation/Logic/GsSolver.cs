﻿using System;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class GsSolver: ISolver
    {
        public int N { get; set; }
        public int Steps { get; set; }
        public SpecialMatrix A { get; set; }
        public Vector<double> B { get; set; }

        public GsSolver(int n, SpecialMatrix a, Vector<double> b)
        {
            N = n;
            A = a;
            B = b;
        }

        public Vector<double> Solve()
        {
            var xgs = new Vector<double>(N); // use only one xgs, init it with i, i = 1,n
            for (var i = 0; i < N; i++)
                xgs.Values[i] = i + 1;
            var step = 0;
            double norm;
            
            do
            {
                norm = 0.0;
                for (var i = 1; i <= N; i++)
                {
                    var rowStartIdx = A.GetRowStart(i - 1) + 1;

                    var result = B[i - 1];
                    result -= GetSumForRowColumns(rowStartIdx, i, 0, i - 1, xgs);
                    result -= GetSumForRowColumns(rowStartIdx, i, i, N, xgs);
                    result /= A.Diagonal[i - 1];

                    norm += Math.Pow(result - xgs[i - 1], 2.0);
                    xgs[i - 1] = result;
                }
                norm = Math.Sqrt(norm);
                ++step;

            } while (step < Utils.MaxStep && norm >= Utils.Precision && norm <= Utils.MaxValueToCallItInfinite);

            Steps = step;

            if(norm < Utils.Precision)
                return xgs;

            if(norm > Utils.MaxValueToCallItInfinite)
                throw  new Exception("Cannot find solution for Ax = b. The sequence tends to go to +Inf ...\nTemp sol: " + xgs);
            
            throw new Exception("Cannot find solution for Ax = b. The number of steps to compute the solution were exceeded.\nTemp sol: " + xgs);
        }

        private double GetSumForRowColumns(int rowIdx, int rowId, int startColumn, int endColumn, Vector<double> xgs)
        {
            var sum = 0.0;
            var currentCell = A.Cells[rowIdx];
            var rowEndCell = new Cell(0, -(rowId + 1));// know where to stop
            var index = rowIdx;

            while (currentCell.Column < startColumn && !currentCell.Equals(rowEndCell)) //find start column
            {
                currentCell = A.Cells[++index];
            }

            while (!currentCell.Equals(rowEndCell) && currentCell.Column < endColumn) // go till end of row or column >= i -1
            {
                sum += currentCell.Value * xgs[currentCell.Column];
                currentCell = A.Cells[++index];
            }

            return sum;
        }
    }
}
