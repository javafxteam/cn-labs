﻿
namespace SeeSharpImplementation.Models
{
    public class Cell
    {
        /// <summary>
        /// Represents the value of the current row, at the given column. Value != 0. If value == 0 => change the row. To see which row is next, check Column value
        /// </summary>
        public double Value { get; set; }

        /// <summary>
        /// Represents the column the current Value resides in. Column smaller than 0 => indicates the index of the next row. The column-row-representation is 1-index-based (starts from 1).
        /// </summary>
        public int Column { get; set; }

        public Cell()
        {
            Value = 0.0;
            Column = -1;
        }

        public Cell(double value, int column)
        {
            Value = value;
            Column = column;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Cell))
                return false;

            var cell = obj as Cell;
            return cell != null && (cell.Column == Column && cell.Value == Value);
        }

        public override string ToString()
        {
            return $"({Value}, {Column})";
        }
    }
}
