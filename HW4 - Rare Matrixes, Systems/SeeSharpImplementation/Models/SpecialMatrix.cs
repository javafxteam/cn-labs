﻿
using SeeSharpImplementation.Logic;
using System;
using System.Globalization;
using System.Linq;

namespace SeeSharpImplementation.Models
{
    public class SpecialMatrix
    {
        private Backgrounder _worker;//represents the worker that runs me. ref it to make him update
        private int[] _rowsStart;

        public int N { get; set; }

        public int Nn { get; set; }
        public Vector<double> Diagonal { get; set; }
        public Vector<Cell> Cells { get; set; }


        public SpecialMatrix(int n, string inputLines, Backgrounder worker)
        {
            _worker = worker;

            N = n;
			Diagonal = new Vector<double>(N);

			// init cells, with empty rows
            Cells = new Vector<Cell>();
            _rowsStart = new int[N + 1];
            for (var i = 1; i <= N + 1; i++)
            {
                Cells.Values.Add(new Cell(0, -i));
				_rowsStart[i - 1] = i - 1;// this means the row (i) starts at i - 1 for now
            }

            var values = inputLines.Split(new[] { "\n\r", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
            Nn = values.Length;

            // start transforming
            _worker.AddLogMessage("Reading Matrix.");
            _worker.UpdateProgress(0);
            _worker.UpdateUpperBounds(Nn + 1);

            for (var i = 0; i < Nn; i++)
            {
                var data = values[i].Split(new[] {", "}, StringSplitOptions.RemoveEmptyEntries);
                var value = double.Parse(data[0], NumberStyles.Any, CultureInfo.InvariantCulture);
                var row = int.Parse(data[1]);
                var column = int.Parse(data[2]);
				InsertValue(value, row, column);

                _worker.UpdateProgress(i + 1);
            }

            // check if elements from diagonal missing
            var diffs = Diagonal.Values.Count(e => Math.Abs(e) < Utils.Precision);
            if (diffs != 0)
                throw new Exception($"All the elements from the diagonal should be != 0.\nElements == 0 (Epsilon-wise) found: {diffs}.");
        }

        public int GetRowStart(int row)
        {
            //var startRowIdx = Cells.Values.IndexOf(new Cell(0, -(row + 1)));
            return _rowsStart[row];
        }

        private void InsertValue(double value, int row, int column)
        {
            if (row == column)
            {
                // check for almost zero elements
                if (Math.Abs(value) < Utils.Precision)
                    throw new Exception($"Found element < epsilon on diagonal at ({row},{row}).");

                Diagonal[row] += value;
                return;
            }

            // IndexOf kills  me here 😧
            var startRowIdx = GetRowStart(row);
            for (var i = row + 1; i < _rowsStart.Length; i++)//update new beginnings
                _rowsStart[i]++;

            var index = startRowIdx + 1;
            Cell currentCell;
            var endCell = new Cell(0, -(row + 2));

            do
            {
                //var newCell = new Cell(value, column);//used when not caring about double entries
                // Cells.Values.Insert(index, newCell);

                currentCell = Cells.Values[index];
                // update
                if (currentCell.Column == column)
                {
                    currentCell.Value += value;
                    break;
                }

                // add only at the end of the row OR column-sorted
                if (currentCell.Column > column || currentCell.Equals(endCell))
                {
                    var newCell = new Cell(value, column);
                    Cells.Values.Insert(index, newCell);
                    break;
                }
                index++;
            }
            while (!currentCell.Equals(endCell));
        }

        public override string ToString()
        {
            return $"N: {N}, " + $"NN + N: {Nn}\n" + "Diagonal: " + Diagonal + "\n" + "Values: " + Cells;
        }
    }
}
