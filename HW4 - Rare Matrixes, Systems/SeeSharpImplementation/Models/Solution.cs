﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Solution
    {
        private ISolver _solver;
        private Vector<double> X;

        public Solution(ISolver solver)
        {
            _solver = solver;
            X = _solver.Solve();
        }

        public bool IsSolutionOk(out IEnumerable<double> diffs, out double norm)
        {
            // modify this
            var n = _solver.N;
            var Y = new Vector<double>(n);
            int i;
            for (i = 1; i <= n; i++)
            {
                Y[i - 1] = 0d;
                var rowStartIdx = _solver.A.GetRowStart(i - 1) + 1;
                var currentCell = _solver.A.Cells[rowStartIdx];
                var rowEndCell = new Cell(0, -(i + 1));// know where to stop
                var index = rowStartIdx;

                while (!currentCell.Equals(rowEndCell)) // go through the columns of the matrix
                {
                    Y[i - 1] += currentCell.Value * X[currentCell.Column];
                    currentCell = _solver.A.Cells[++index];
                }

                // do it with the diagonal element too
                Y[i - 1] += _solver.A.Diagonal[i - 1] * X[i - 1];

            }
            var temp = new List<double>();
            for(i = 1; i <= n; i ++)
                temp.Add(Y[i - 1] - _solver.B[i - 1]);
            diffs = temp;

            norm = Utils.GetInfinityNorm(diffs);
            return !(norm >= Utils.Precision);
        }

        public override string ToString()
        {
            IEnumerable<double> diffs;
            double norm;
            var isOk = IsSolutionOk(out diffs, out norm);
            var diffstring = "Differences vector: " + string.Join(" ", diffs.Take(5).Select(o => o.ToString(CultureInfo.InvariantCulture))) + $" [{diffs.Count()} elements]\n";
            var text = isOk ? $"Solution is OK (norm < 10^-{Utils.PrecisionPower}).\n" : $"Solution is not OK (norm >= 10^-{Utils.PrecisionPower}).\n";
            var normstring = "Norm(inf) (||Y - B||): " + norm.ToString(CultureInfo.InvariantCulture) + "\n";
            var stepsString = "Steps required: " + _solver.Steps + ".\n";
            return "X: (" + string.Join(", ", X) + ")\n" +
                text + diffstring + normstring + stepsString;
        }
    }
}
