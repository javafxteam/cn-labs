﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeSharpImplementation.Models
{
    public class Vector<T>
        where T: new()
    {
        public List<T> Values { get; set; } = new List<T>();

        /// <summary>
        /// Returns the size of the vector.
        /// </summary>
        public int Count => Values.Count;


        public Vector(){ }

        public Vector(int capacity)
        {
            Values = new List<T>(capacity);
            for (var i = 0; i < capacity; i++)
            {
                Values.Add(new T());
            }
        }

        
        public T this[int index]
        {
            get
            {
                if(index < 0 || index >= Values.Count)
                    throw new ArgumentException();
                return Values[index];
            }
            set
            {
                if (index < 0 || index >= Values.Capacity)
                    throw new ArgumentException();
                if (index < Values.Count)
                    Values[index] = value;
                else
                    Values.Insert(index, value);
            }
        }

        public override string ToString()
        {
            return string.Join(", ", Values.Take(5).Select(x => x.ToString())) + $" [{Values.Count} elements].";
        }
    }
}
