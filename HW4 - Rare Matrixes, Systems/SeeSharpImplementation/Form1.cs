﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SeeSharpImplementation.Logic;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Properties;

namespace SeeSharpImplementation
{
    public partial class MainWindow : Form
    {
        private Controller _controller;
        private Color LOG_TEXT = Color.Gray;
        private Color IMPORTANT_TEXT = Color.Crimson;
        private Color NICE_TEXT = Color.DarkGreen;
        

        public MainWindow()
        {
            InitializeComponent();
            SetUpMiddleScreenMiddleSize();
        }

        private void SetUpMiddleScreenMiddleSize()
        {
            var cScreen = Screen.FromControl(this);
            //var newSize = new Size(cScreen.Bounds.Width / 2, cScreen.Bounds.Height / 2);
            var newLocation = new Point((int)(1.0 / 4 * cScreen.Bounds.Width), (int)(1.0 / 4 * cScreen.Bounds.Height));
            //Size = newSize;
            Location = newLocation;

            Utils.ComputeSystemPrecisionPower();
            logTB.AppendText($"Precision of the system computed. It's {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n", LOG_TEXT);
            precisionUpDown.Maximum = Utils.PrecisionPower;
            precisionUpDown.Value = 8;
            precisionLabel.Text = $"Precision (sys max: {Utils.PrecisionPower}).";
            logTB.Font = new Font(FontFamily.GenericMonospace, logTB.Font.Size);
        }
        
        private void precisionUpDown_ValueChanged(object sender, EventArgs e)
        {
            Utils.PrecisionPower = (int)precisionUpDown.Value;
            Utils.Precision = Math.Pow(10, -Utils.PrecisionPower);
            Utils.MaxStep = (int)Math.Pow(10, Utils.PrecisionPower / 2);
            //Utils.MaxValueToCallItInfinite = Math.Pow(10, Utils.PrecisionPower);
            logTB.AppendText($"Precision set to {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n");
            logTB.AppendText($"Max Value to decide wether the sequence converges to +Inf or not is {Utils.MaxValueToCallItInfinite} (10^({Utils.PrecisionPower})).\n");
            logTB.AppendText($"To find the solution I will go a maximum of {Utils.MaxStep} steps.\n");
        }

        private void _clearItemsBtn_Click(object sender, EventArgs e)
        {
            _loadedItemsTB.DataSource = null;
            logTB.AppendText($"Items cleared.\n", LOG_TEXT);
        }

        private void _loadItemsBTN_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = true,
                Filter = Resources.res_MainWindowOFDFilter,
                InitialDirectory = Application.StartupPath
            };
            var result = ofd.ShowDialog();
            if (result != DialogResult.OK) return;

            var files = ofd.FileNames;
            var realFiles = files.Select(file => new FileInfo(file)).ToList();

            _loadedItemsTB.DataSource = realFiles;
            _loadedItemsTB.DisplayMember = "Name";

            logTB.AppendText($"{realFiles.Count} items loaded.\n", LOG_TEXT);
        }


        private void computeBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if(_loadedItemsTB.SelectedItem == null)
                    throw new Exception("Please load and select an item first, kind human.");
                var fileInfo = _loadedItemsTB.SelectedItem as FileInfo;

                // Step 1 - Read Data in memory
                LoadDataInMemory(fileInfo, () =>
                {
                    try
                    {
                        // find solution
                        var repr = _controller.GetSolution();
                        logTB.AppendText($"The Ax = b system solution:\n{repr}\n", IMPORTANT_TEXT);
                    }
                    catch (Exception exception)
                    {
                        logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                        logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                        logTB.ShowWarningFromException(exception);
                    }

                });

            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }

        private void LoadDataInMemory(FileInfo fileInfo, Action next)
        {
            var filePath = fileInfo.FullName;
            logTB.AppendText($"\n\nReading items from {fileInfo.Name}...\n", LOG_TEXT);

            var executer = new Backgrounder(_workProgress, logTB);

            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller = new Controller(filePath, executer);
            };
            executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText($"Data loaded in memory.\n", NICE_TEXT);
                logTB.AppendText($"N: {_controller.N}\n", IMPORTANT_TEXT);
                logTB.AppendText($"B: {_controller.B}\n", IMPORTANT_TEXT);
                logTB.AppendText($"Matrix:\n{_controller.A}\n", NICE_TEXT);

                // call next
                next();
            };
            executer.SetOnWorkerCompleted(onCompleted);

            executer.Start();
        }
    }
}
