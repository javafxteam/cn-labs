﻿
using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using SeeSharpImplementation.Models;
using SeeSharpImplementation.Properties;

namespace SeeSharpImplementation.Extensions
{
    public static class HExtensions
    {
        // thread safe append text
       public static void AppendText(this RichTextBox box, string text, Color color)
        {
            if (box.InvokeRequired)
            {
                box.Invoke(new Action<RichTextBox, string, Color>(AppendText), box, text, color);
            }
            else
            {
                box.SelectionStart = box.TextLength;
                box.SelectionLength = 0;

                box.SelectionColor = color;
                box.AppendText(text);
                box.SelectionColor = box.ForeColor;
                box.ScrollToCaret();
            }
        }

        // thread safe SetProgress
        public static void SetProgress(this ProgressBar pb, int value)
        {
            if(pb.InvokeRequired)
            {
                pb.Invoke(new Action<ProgressBar, int>(SetProgress), pb, value);
            }
            else
            {
                pb.Value = value;
            }
        }

        // thread safe SetMax
        public static void SetMaxValue(this ProgressBar pb, int value)
        {
            if (pb.InvokeRequired)
            {
                pb.Invoke(new Action<ProgressBar, int>(SetMaxValue), pb, value);
            }
            else
            {
                pb.Maximum = value;
            }
        }

        // thread safe show exception message
        public static void ShowWarningFromException(this RichTextBox tb, Exception exception)
        {
            if (tb.InvokeRequired)
                tb.Invoke(new Action<RichTextBox, Exception>(ShowWarningFromException), tb, exception);
            else
                MessageBox.Show(tb, exception.Message, Resources.res_ExceptionHappened, MessageBoxButtons.OK,
                MessageBoxIcon.Warning);
        }

        /// <summary>
        /// The StreamReader goes N lines forward and returns the string.
        /// </summary>
        /// <param name="n">The number of lines to read.</param>
        /// <returns></returns>
        public static string ReadMaxNLines(this StreamReader reader, int n)
        {
            var output = "";
            for (var i = 0; i < n; i++)
            {
                output += reader.ReadLine() + "\n";
            }
            return output;
        }

        /// <summary>
        /// Given the input string in the form of an array sepparated by spaces, it builds the current vector from that string
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="inputSeparatedByLineBreaks">Possible input: '1\n2\n3'</param>
        /// <param name="id">Represents the id of this vector, to help identify if possible exceptions.</param>
        public static void BuildFromString(this Vector<double> vec, string inputSeparatedByLineBreaks, string id = "unknown")
        {
            var n = vec.Values.Capacity;
            var values = inputSeparatedByLineBreaks.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (values.Length != n)
                throw new Exception($"The number of rows is not {n}. Error occurred on: {id}.");

            for (var j = 0; j < values.Length; j++)
            {
                try
                {
                    vec[j] = double.Parse(values[j], NumberStyles.Any, CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    throw new Exception($"You must insert double numbers. Error occurred on: {id}.");
                }
            }
        }
    }
}
