package logic;

/**
 * Created on 06.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class PolyInfo implements Comparable<PolyInfo> {

    private int index;
    private double average_error;
    private double runTime;

    public PolyInfo(int index) {
        this.index = index;
    }

    public PolyInfo(int index, double average_error, double run_time) {

        this.index = index;
        this.average_error = average_error;
        this.runTime = run_time;
    }

    public String getName() { return "P" + index; }

    public double getRunTime() { return runTime; }

    public void setAverageError(double average_error) { this.average_error = average_error; }

    public void setRunTime(double runTime) { this.runTime = runTime; }

    @Override
    public int compareTo(PolyInfo o) {
        return Double.compare(this.average_error, o.average_error);
    }
}