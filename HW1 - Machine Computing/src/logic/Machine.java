package logic;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

/**
 * Created on 2/24/2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Machine {

    public final static Polynomial p1 = new Polynomial(1)
            .addTerm(1,1)
            .addTerm(3, -Constant.ONE.getValue())
            .addTerm(5, Constant.TWO.getValue());
    public final static Polynomial p2 = p1.copy(2).addTerm(7, -Constant.THREE.getValue());
    public final static Polynomial p3 = p2.copy(3).addTerm(9, Constant.FOUR.getValue());
    public final static Polynomial p4 = new Polynomial(4)
            .addTerm(1, 1)
            .addTerm(3, -0.166)
            .addTerm(5,  0.00833)
            .addTerm(7, -Constant.THREE.getValue())
            .addTerm(9, Constant.FOUR.getValue());
    public static Polynomial p5 = p3.copy(5).addTerm(11, -Constant.FIVE.getValue());
    public static Polynomial p6 = p5.copy(6).addTerm(13, Constant.SIX.getValue());

    public static void main(String[] args)
    {

        double precision = Machine.getPrecision();
        out.println("Precision: " + precision);
        out.println("Is addition associative? " + Machine.isAdditionAssociative());

        Numbers numbers = Machine.getMultiplicationIsAssociativeCounterExample();
        out.println("Numbers for which multiplication is not associative: "
                + numbers.getX() + " " + numbers.getY() + " " + numbers.getZ());

        out.println(p1);
        out.println(p2);
        out.println(p3);
        out.println(p4);
        out.println(p5);
        out.println(p6);

        List<Polynomial> polynomials = Arrays.asList(Machine.p1, Machine.p2, Machine.p3, Machine.p4, Machine.p5, Machine.p6);
        for(int i = 1; i <= 5; ++i)
        {

            List<PolyInfo> errors = SinusUtils.computeAverageErrorsFor(10000, true, polynomials);

            out.println("Errors and time for iteration: " + i);
            for (PolyInfo error : errors) {
                out.println(error.getName());
                out.println(error.getRunTime());
                out.println();
            }
        }

    }

    public static double getPrecision()
    {
        double precision = 1d/10;
        double constant = 1.0;

        while(constant + precision != constant)
        {
            precision /= 10;
        }

        return precision * 10;
    }

    public static boolean isAdditionAssociative()
    {
        double precision = Machine.getPrecision();

        double x = 1.0, y = precision, z = precision;

        return (x + y) + z == x + (y + z);
    }

    public static Numbers getMultiplicationIsAssociativeCounterExample()
    {
        Numbers numbers = new Numbers();

        while(numbers.isMultiplicationAssociative())
        {
            numbers.changeState();
        }

        return numbers;
    }
}
