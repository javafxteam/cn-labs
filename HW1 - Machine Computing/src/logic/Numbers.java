package logic;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Created on 2/24/2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Numbers {

    private Random generator = new Random();

    private double x;
    private double y;
    private double z;

    public Numbers()
    {
        changeState();
    }

    public void changeState()
    {
        x = generator.nextDouble();
        y = generator.nextDouble();
        z = generator.nextDouble();
    }

    public boolean isMultiplicationAssociative()
    {
        return (x * y) * z == x * (y * z);
    }

    public double getX()
    {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public BigDecimal getBigX() { return new BigDecimal(x); }

    public BigDecimal getBigY() { return new BigDecimal(y); }

    public BigDecimal getBigZ() { return new BigDecimal(z); }
}
