package logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created on 2/24/2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class SinusUtils {

    private static List<Double> generateRandomValues(double min, double max, int quantity) {
        Random random = new Random();

        return random.doubles(min, max).limit(quantity).mapToObj(Double::new)
                .collect(Collectors.toList());
    }

    public static List<PolyInfo>
    computeAverageErrorsFor(int values_quantity, boolean useSimplifiedForm, List<Polynomial> polynomials) {

        int nr = polynomials.size();
        List<PolyInfo> infos = new ArrayList<>();

        double max = Math.PI / 2;
        double min = -max;
        List<Double> values = generateRandomValues(min, max, values_quantity);

        for(int i = 0; i < nr; i++) {
            PolyInfo info = getAverageErrorFor(polynomials.get(i), useSimplifiedForm, values);
            infos.add(info);
        }

        return infos;
    }

    public static PolyInfo getAverageErrorFor(Polynomial polynomial, boolean useSimplifiedForm, List<Double> values) {

        int quantity = values.size();

        PolyInfo info = new PolyInfo(polynomial.getIndex());

        double sum_error = values.stream()
                .reduce(0.0, (acc, number) -> {
                    double polynomial_sinus;
                    long start = System.nanoTime();
                    if(useSimplifiedForm)
                        polynomial_sinus = polynomial.evaluateSimplifiedFor(number);
                    else
                        polynomial_sinus = polynomial.evaluateFor(number);
                    long end = System.nanoTime();
                    double millis = (end - start) / 1000000.0;
                    info.setRunTime(info.getRunTime() + millis);
                    double real_sinus = Math.sin(number);
                    return acc + (Math.abs(polynomial_sinus - real_sinus));
                });

        info.setRunTime(info.getRunTime());
        info.setAverageError(sum_error / quantity);
        return info;
    }
}

