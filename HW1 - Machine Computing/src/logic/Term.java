package logic;

import java.util.stream.IntStream;

/**
 * Created on 03.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Term implements Comparable<Term> {

    private int exp;
    private double coeff;

    public Term(int exp, double coeff) {
        this.exp = exp;
        this.coeff = coeff;
    }

    public int getExp() {
        return exp;
    }

    public double getCoeff() {
        return coeff;
    }

    @Override
    public int compareTo(Term other) {
        return Integer.compare(this.exp, other.exp);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if(coeff > 0)
            sb.append("+");

        if(coeff != 0)
            sb.append(coeff);

        sb.append("x");

        if(exp != 1) {
            IntStream digits = (String.valueOf(exp).chars());
            digits.forEach(digit -> sb.append(superscript(digit)));
        }

        return sb.toString();
    }

    private char superscript(int digit) {

        switch(digit){
            case '0': return '⁰';
            case '1': return '¹';
            case '2': return '²';
            case '3': return '³';
            case '4': return '⁴';
            case '5': return '⁵';
            case '6': return '⁶';
            case '7': return '⁷';
            case '8': return '⁸';
            case '9': return '⁹';
        }

        return ' ';
    }
}
