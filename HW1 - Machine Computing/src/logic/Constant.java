package logic;

/**
 * Created on 2/24/2017.
 * @author Cezara C.
 * @author Marian F.
 */

public enum Constant {

    ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6);

    private int index;

    private double value;

    Constant(int number) {
        this.index = number;
        value = this.computeValue();
    }

    public double getValue()
    {
        return value;
    }

    private double computeValue()
    {
        int max = 2 * index + 1;
        return 1.0 / factorial(max);
    }

    private double factorial(int max)
    {
        double result = 1.0;
        for(int i = 2; i <= max; i++)
            result *= i;

        return result;
    }
}

