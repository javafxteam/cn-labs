package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created on 2/24/2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Polynomial {

    private int index;
    private List<Term> terms = new ArrayList<>();

    public Polynomial(int index) {
        this.index = index;
    }

    public Polynomial addTerm(Term term) {
        terms.add(term);
        return this;
    }

    public Polynomial addTerm(int exp, double coeff) {
        terms.add(new Term(exp, coeff));
        return this;
    }

    public double evaluateFor(double x) {

        double result = 0;
        for (Term term : terms) {
            int exp = term.getExp();
            double coeff = term.getCoeff();

            result += coeff * Math.pow(x, exp);
        }

        return result;
    }

    public double evaluateSimplifiedFor(double x) {

        double square = x  * x;
        int end = terms.size() - 1;
        double parenthesis = terms.get(end--).getCoeff() * square + terms.get(end--).getCoeff();

        for(int i = end; i >= 0; --i) {
            parenthesis = terms.get(i).getCoeff() + square * parenthesis;
        }

        return x * parenthesis;
    }

    public Polynomial copy(int new_index) {
        Polynomial copy = new Polynomial(new_index);
        for (Term term : terms)
            copy.addTerm(term);

        return copy;
    }

    public int getIndex() { return index; }

    @Override
    public String toString() {
        Collections.sort(terms);
        StringBuilder sb = new StringBuilder("P" + index);
        sb.append(" = ");
        for (Term term : terms)
            sb.append(term);

        return sb.toString();
    }
}
