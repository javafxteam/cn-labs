package views;

import javafx.scene.control.Label;

/**
 * Created on 05.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class NumberLabel extends Label {

    private String label;

    public NumberLabel(String label_name) {

        super(label_name);
        label = label_name;
    }

    public void reset() {
        setText(label);
    }
}
