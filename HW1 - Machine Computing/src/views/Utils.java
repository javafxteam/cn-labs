package views;

import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

/**
 * Created on 05.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Utils {

    public interface NodeCaller {
        void apply(Node node);
    }

    public static VBox getVBoxContainer() {
        VBox box = new VBox();
        box.getStyleClass().add("v-box");
        return box;
    }

    //like an extension method?
    public static void onTabChange(Tab tab, Node node, NodeCaller caller) {
        tab.getTabPane().getSelectionModel()
                .selectedItemProperty()
                .addListener((obs, oldTab, newTab) -> {

                    if(!oldTab.equals(newTab) && oldTab.equals(tab))
                        caller.apply(node);
                });
    }


}
