package views;

import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Created on 04.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Window extends Stage {

    public Window() {

        super();

        TabPane tab_pane = new TabPane();
        tab_pane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tab_pane.setSide(Side.BOTTOM);

        PrecisionTab precision_tab = new PrecisionTab();
        OperationsPropertiesTab op_tab = new OperationsPropertiesTab();
        PolynomialChartTab poly_tab = new PolynomialChartTab();

        Scene scene = new Scene(tab_pane);
        setScene(scene);

        tab_pane.getTabs().addAll(precision_tab, op_tab, poly_tab);
        precision_tab.decorate();
        op_tab.decorate();
        poly_tab.decorate();

        setup();

        //events
        tab_pane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
            if(!oldTab.equals(newTab))
                setTitle(newTab.getText() + " - Machine Info-metrics");
        });
    }

    private void setup() {

        this.setTitle("Precision - Machine Info-metrics");

        Image icon = new Image("precision_icon.jpg");
        this.getIcons().add(icon);

        setHeight(400);
        setWidth(600);

        getScene().getStylesheets().add("stylesheet.css");
    }
}
