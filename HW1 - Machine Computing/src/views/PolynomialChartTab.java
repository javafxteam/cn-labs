package views;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import logic.Machine;
import logic.PolyInfo;
import logic.Polynomial;
import logic.SinusUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created on 05.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class PolynomialChartTab extends Tab {

    final BarChart barChart;

    public PolynomialChartTab() {

        super();
        setText("Sinus");

        final CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Polynomial");

        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Time (millis)");

        barChart = new BarChart<>(xAxis, yAxis);
        barChart.setTitle("Polynomials Run Times");

    }

    public void decorate() {

        TextField quantity_field = new TextField();
        quantity_field.setPromptText("Quantity");
        Tooltip quantity_field_tooltip = new Tooltip("How many random values to generate for drawing the chart.");
        quantity_field.setTooltip(quantity_field_tooltip);

        Button draw_chart_btn = new Button("Draw chart");

        HBox box = new HBox();
        box.getStyleClass().add("h-box");
        box.getChildren().addAll(quantity_field, draw_chart_btn);

        BorderPane pane = new BorderPane();
        pane.getStyleClass().add("v-box");
        pane.setTop(box);
        pane.setCenter(barChart);
        setContent(pane);

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("NormalForm");

        //events
        draw_chart_btn.setOnMouseClicked(mouseEvent -> drawChart(Integer.parseInt(quantity_field.getText())));

        Utils.onTabChange(this, quantity_field, node -> ((TextField)node).clear());
        Utils.onTabChange(this, barChart, node -> ((BarChart)node).getData().clear());
    }

    private void drawChart(int quantity) {

        barChart.getData().clear();

        List<Polynomial> polynomials = Arrays.asList(Machine.p1, Machine.p2, Machine.p3, Machine.p4, Machine.p5, Machine.p6);

        List<XYChart.Series> series = Arrays.asList(new XYChart.Series(), new XYChart.Series());
        series.get(0).setName("Normal Form");
        series.get(1).setName("Simplified Form");

        List<Boolean> useSimplifiedForm = Arrays.asList(false, true);

        for(int s = 0; s < 2; ++s) {

            List<PolyInfo> infos = null;
            for(int i = 0; i < 2; ++i) {
                infos = SinusUtils.computeAverageErrorsFor(quantity, useSimplifiedForm.get(s), polynomials);
            }
            Collections.sort(infos);

            for (PolyInfo info : infos) {
                series.get(s).getData().add(new XYChart.Data<>(info.getName(), info.getRunTime()));
            }

            barChart.getData().add(series.get(s));
        }
    }
}
