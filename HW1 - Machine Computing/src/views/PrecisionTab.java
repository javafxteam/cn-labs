package views;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import logic.Machine;

/**
 * Created on 04.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
class PrecisionTab extends Tab {

    PrecisionTab() {
        super("Precision");
    }

    void decorate() {

        VBox box = Utils.getVBoxContainer();

        Button precision_btn = new Button("Compute");

        Label precision_label = new Label();
        precision_label.setVisible(false);
        precision_label.setAlignment(Pos.CENTER);

        box.widthProperty().addListener((observable, oldValue, newValue) -> {
            double value = (newValue.doubleValue() - 14) / 2;
            precision_btn.setMinWidth(value);
            precision_label.setMinWidth(value);
        });

        box.heightProperty().addListener((observable, oldValue, newValue) -> {
            double value = (newValue.doubleValue() - 14) / 3;
            precision_btn.setMinHeight(value);
            precision_label.setMinHeight(value);
        });

        DoubleProperty fontSize = new SimpleDoubleProperty(50);
        fontSize.bind(precision_btn.widthProperty().add(precision_btn.heightProperty()).divide(10));
        box.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSize.asString()));

        box.getChildren().addAll(precision_btn, precision_label);

        setContent(box);

        //events
        precision_btn.setOnMouseClicked(mouseEvent -> {
            precision_label.setText(String.valueOf(Machine.getPrecision()));
            precision_label.setVisible(true);
        });

        Utils.onTabChange(this, precision_label, node -> node.setVisible(false));
    }
}
