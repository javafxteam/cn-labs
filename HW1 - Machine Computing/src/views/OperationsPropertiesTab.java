package views;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import logic.Machine;
import logic.Numbers;

/**
 * Created on 05.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class OperationsPropertiesTab extends Tab {

    public OperationsPropertiesTab() {
        super("Operations and Properties");
    }

    public void decorate() {

        VBox left_box = Utils.getVBoxContainer();
        VBox right_box = Utils.getVBoxContainer();

        SplitPane split_pane = new SplitPane();
        split_pane.getItems().addAll(left_box, right_box);

        //lock divider position
        left_box.minWidthProperty().bind(split_pane.widthProperty().multiply(0.5));
        left_box.maxWidthProperty().bind(split_pane.widthProperty().multiply(0.5));

        setContent(split_pane);

        //left nodes
        Label question_mark_label = new Label("?");
        question_mark_label.setAlignment(Pos.BOTTOM_CENTER);
        Label addition_statement_label = new Label("(1.0 + p) + p = 1.0 + (p + p)");
        addition_statement_label.setAlignment(Pos.TOP_CENTER);

        Button test_addition_btn = new Button("Test");

        Label addition_result_label = new Label();
        addition_result_label.setVisible(false);
        addition_result_label.setAlignment(Pos.CENTER);

        left_box.getChildren().addAll(question_mark_label, addition_statement_label, test_addition_btn, addition_result_label);

        //right nodes
        Button get_btn = new Button("Get");
        Text text = new Text("Press 'Get' to find 3 random numbers for which multiplication is not associative.");
        text.wrappingWidthProperty().bind(getTabPane().getScene().widthProperty().divide(2).subtract(40));
        text.setTextAlignment(TextAlignment.CENTER);
        Label box1 = new NumberLabel("a = ");
        Label box2 = new NumberLabel("b = ");
        Label box3 = new NumberLabel("c = ");

        right_box.getChildren().addAll(text, get_btn, box1, box2, box3);

        //sizing
        left_box.widthProperty().addListener((observable, oldValue, newValue) -> {
            double value = (newValue.doubleValue() - 14);
            test_addition_btn.setMinWidth(value / 2);
            get_btn.setMinWidth(value / 2);
            left_box.lookupAll(".label").forEach(child -> ((Region)child).setMinWidth(value - 10));
        });

        /*right_box.widthProperty().addListener((observable, oldValue, newValue) -> {
            double value = (newValue.doubleValue() - 14);
            //test_mul_btn.setMinWidth(value / 2);
            right_box.lookupAll(".label").forEach(child -> ((Region)child).setMinWidth(value - 10));
        });*/

        left_box.heightProperty().addListener((observable, oldValue, newValue) -> {
            double value = (newValue.doubleValue() - 44) / 7.5; // nr of nodes * 1.5
            split_pane.getItems().forEach(box -> ((VBox)box).getChildren().forEach(child -> {
                if(child instanceof Region) {
                    ((Region)child).setMinHeight(value);
                }
            }));
        });

        //fonts
        DoubleProperty fontSizeBtn = new SimpleDoubleProperty(50);
        fontSizeBtn.bind(test_addition_btn.widthProperty().add(test_addition_btn.heightProperty()).divide(8));
        test_addition_btn.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSizeBtn.asString()));
        get_btn.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSizeBtn.asString()));

        DoubleProperty fontSizeLabels = new SimpleDoubleProperty(50);
        fontSizeLabels.bind(addition_statement_label.widthProperty().add(addition_statement_label.heightProperty()).divide(15));
        left_box.lookupAll(".label").forEach(child ->
                child.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSizeLabels.asString())));

        DoubleProperty fontSizeRightLabels = new SimpleDoubleProperty(50);
        fontSizeRightLabels.bind(right_box.widthProperty().add(right_box.heightProperty()).divide(30));

        right_box.lookupAll(".label").forEach(child ->
                child.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSizeLabels.asString())));

        //events
        test_addition_btn.setOnMouseClicked(mouseEvent -> {
            addition_result_label.setText(String.valueOf(Machine.isAdditionAssociative()).toUpperCase());
            addition_result_label.setVisible(true);
        });

        get_btn.setOnMouseClicked(mouseEvent -> {

            Numbers numbers = Machine.getMultiplicationIsAssociativeCounterExample();
            double a = numbers.getX(), b = numbers.getY(), c = numbers.getZ();
            box1.setText("a = " + a);
            box2.setText("b = " + b);
            box3.setText("c = " + c);
        });


        Utils.onTabChange(this, addition_result_label, node -> node.setVisible(false));
        Utils.onTabChange(this, right_box, box -> ((VBox)box).lookupAll(".label")
                .forEach(child -> ((NumberLabel)child).reset()));
    }
}
