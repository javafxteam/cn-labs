﻿
using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Factorization;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class Decomposer
    {
        private int n;
        private Matrix<double> A;
        private Vector<double> D;

        public Decomposer(int n, Matrix<double> A)
        {
            this.n = n;
            this.A = A;
            D = new DenseVector(n);
        }

        /// <summary>
        /// Runs the LDLt decomposition, and then outputs the Diagonal vector in D and puts the L matrix in the lower triangle of the original A matrix.
        /// </summary>
        public Ldl DecomposeLdl()
        {
            for (var p = 1; p <= n; p++)
            {
                SetDp(p);
                SetColumnPinL(p);
            }
            return new Ldl(n, A, D);
        }

        /// <summary>
        /// Runs the Math.Numerics' build in LU decomposition (on the original A matrix).
        /// </summary>
        /// <returns></returns>
        public LU<double> DecomposeLu()
        {
            var aRebuild = Utils.GetAInit(A);
            return aRebuild.LU();
        }

        /// <summary>
        /// The methods computes and sets Dp, given step p, for the decomposition.
        /// </summary>
        private void SetDp(int p)
        {
            var sum = 0.0;
            for (var k = 1; k <= p - 1; k++)
                sum += D[k - 1] * Math.Pow(Utils.GetFromL(A, p, k), 2); 
            D[p - 1] = Utils.GetFromA(A, p, p) - sum;

            try
            {
                Utils.CheckIfZero(D[p - 1], Utils.Precision);
            }
            catch (Exception e)
            {
                throw  new Exception($"Cannot decompose. Found the element D{p} equal to Zero according to the precision. Inner details: {e.Message}.");
            }
        }

        /// <summary>
        /// The methods computes and sets the column p of the L lower-triangle matrix, given step p, for the decomposition (and saves it in the lower-triangle of the A matrix).
        /// </summary>
        private void SetColumnPinL(int p)
        {
            for (var i = p + 1; i <= n; i++)
            {
                var sum = 0.0;
                for (var k = 1; k <= p - 1; k++)
                    sum += D[k - 1] * Utils.GetFromL(A, i, k) * Utils.GetFromL(A, p, k);
                var result =  (Utils.GetFromA(A, i, p) - sum) / D[p - 1];
                A[i - 1, p - 1] = result;
            }
        }
    }
}
