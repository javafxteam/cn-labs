﻿
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class Solver: ISolver
    {
        public int N { get; set; }
        public Matrix<double> A { get; set; }
        public Vector<double> B { get; set; }

        private Vector<double> D;

        public Solver(Ldl decomposition, Vector<double> B)
        {
            this.N = decomposition.N;
            this.A = decomposition.A;
            this.D = decomposition.D;
            this.B = B;
        }

        public Vector<double> Solve()
        {
            var z = SolveSystemLzb();
            var y = SolveSystemDyz(z);
            var x = SolveSystemLtxy(y);
            return x;
        }

        private Vector<double> SolveSystemLzb()
        {
            var z = new DenseVector(N);
            for (var i = 1; i <= N; i++)
            {
                var sum = 0.0d;
                for (var j = 1; j <= i - 1; j++)
                    sum += Utils.GetFromL(A, i, j) * z[j - 1];
                z[i - 1] = (B[i - 1] - sum); // / L[i, i], which is 1
            }
            return z;
        }

        private Vector<double> SolveSystemDyz(Vector<double> z)
        {
            var y = new DenseVector(N);
            for (var i = 0; i < N; i++)
                y[i] = z[i] / D[i];
            return y;
        }

        private Vector<double> SolveSystemLtxy(Vector<double> y)
        {
            var x = new DenseVector(N);
            for (var i = N; i >= 1; i--)
            {
                var sum = 0.0d;
                for (var j = N; j >= i + 1; j--)
                    sum += Utils.GetFromLt(A, i, j) * x[j - 1];
                x[i - 1] = (y[i - 1] - sum); // / Lt[i, i], which is 1
            }
            return x;
        }
    }
}
