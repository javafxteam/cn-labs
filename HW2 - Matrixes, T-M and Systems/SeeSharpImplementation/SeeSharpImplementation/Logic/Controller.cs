﻿using System.Globalization;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Factorization;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class Controller
    {
        private int N;
        private Matrix<double> A;
        private Vector<double> b;

        private Decomposer _decomposer;
        private Ldl ldlDecomposition;
        private LU<double> luDecomposition;
        private Solution _solution;

        public Controller(int n, string matrixAString, string vectorBString)
        {
            N = n;
            A = new DenseMatrix(N, N);
            b = new DenseVector(N);
            A.BuildFromString(matrixAString);
            b.BuildFromString(vectorBString, "b");
            _decomposer = new Decomposer(N, A);
        }

        /// <summary>
        /// Runs the LDL decomposition and returns the matrixes as String.
        /// </summary>
        /// <returns>The string representation of L, D and Lt</returns>
        public string DecomposeLdlAndReturnRepresentation()
        {
            ldlDecomposition = _decomposer.DecomposeLdl();
            return ldlDecomposition.ToString();
        }

        /// <summary>
        /// Returns the string representation of the determinant from LDL. One must run the LDL Decompose first.
        /// </summary>
        /// <returns></returns>
        public string GetLdlDeterminant()
        {
            return ldlDecomposition.GetDeterminant().ToString(CultureInfo.InvariantCulture);
        }

        public string GetSolutionLdl()
        {
            var ldlSolver = new Solver(ldlDecomposition, b);
            _solution = new Solution(ldlSolver);
            return _solution.ToString();
        }

        /// <summary>
        /// Runs the LDL decomposition and returns the matrixes as String.
        /// </summary>
        /// <returns>The string representation of L, D and Lt</returns>
        public string DecomposeLuAndReturnRepresentation()
        {
            luDecomposition = _decomposer.DecomposeLu();
            return luDecomposition.ToStringer();
        }

        public string GetSolutionLu()
        {
            var luSolver = new LuSolver(luDecomposition, A, b);
            _solution = new Solution(luSolver);
            return _solution.ToString();
        }

    }
}
