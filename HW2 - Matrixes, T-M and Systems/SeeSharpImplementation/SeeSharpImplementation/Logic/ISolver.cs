﻿
using MathNet.Numerics.LinearAlgebra;

namespace SeeSharpImplementation.Logic
{
    public interface ISolver
    {
        int N { get; set; }
        Matrix<double> A { get; set; }
        Vector<double> B { get; set; }

        Vector<double> Solve();
    }
}
