﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Factorization;

namespace SeeSharpImplementation.Logic
{
    public class LuSolver: ISolver
    {
        public int N { get; set; }
        public Matrix<double> A { get; set; }
        public Vector<double> B { get; set; }

        private LU<double> _decomposition;

        public LuSolver(LU<double> decomposition, Matrix<double> A, Vector<double> B)
        {
            this._decomposition = decomposition;
            this.N = decomposition.L.RowCount;
            this.A = A;
            this.B = B;
        }

        public Vector<double> Solve()
        {
            return _decomposition.Solve(B);
        }
    }
}
