﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace SeeSharpImplementation.Logic
{
    internal class Utils
    {
        public static double Precision;
        public static int PrecisionPower;

        /// <summary>
        /// Checks if a number if zero given the error-precision to take into account.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="precision"></param>
        public static void CheckIfZero(double number, double precision)
        {
            if (Math.Abs(number) <= precision)
                throw new Exception("That number is so close to Zero, Oh Dear...");
        }

        public static double GetNorm(IEnumerable<double> Z)
        {
            var sum = 0.0;
            Z.ToList().ForEach(x => sum += Math.Pow(x, 2));
            return Math.Sqrt(sum);
        }

        /// <summary>
        /// Gets elements from the matrix L (stored in lower-triangle of A):
        /// - if the elements are in the upper triangle => return 0
        /// - if the elements are on the diagonal => return 1
        /// - if the element is in the lower triangle (diagonal excluded) => return elements from A 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i">The 1-based row index.</param>
        /// <param name="j">The 1-based column index.</param>
        /// <returns></returns>
        public static double GetFromL(Matrix<double> A, int i, int j)
        {
            i--;
            j--;
            // diagonal = 1, upper-triangle = 0, rest = in A.
            return i == j ? 1.0 : i <= j ? 0.0 : A[i, j];
        }

        /// <summary>
        /// Gets elements from the matrix Lt (the triangle-symmetric of L):
        /// - returns L[j, i]
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i">The 1-based row index.</param>
        /// <param name="j">The 1-based column index.</param>
        /// <returns></returns>
        public static double GetFromLt(Matrix<double> A, int i, int j)
        {
            return GetFromL(A, j, i);
        }

        /// <summary>
        /// Gets elements from the matrix A:
        /// - if the elements are in the upper triangle => return the element at given position
        /// - if the element is in the lower triangle (diagonal excluded) => return elements from A[j,i] (symmetric) 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i">The 1-based row index.</param>
        /// <param name="j">The 1-based column index.</param>
        /// <returns></returns>
        public static double GetFromA(Matrix<double> A, int i, int j)
        {
            i--;
            j--;
            return i > j ? A[j, i] : A[i, j];
        }

        public static Matrix<double> GetAInit(Matrix<double> A)
        {
            var n = A.RowCount;
            Matrix<double> aRebuild = new DenseMatrix(n, n);
            for (var i = 1; i <= n; i++)
                for (var j = 1; j <= n; j++)
                    aRebuild[i - 1, j - 1] = GetFromA(A, i, j);
            return aRebuild;
        }

        /// <summary>
        /// Given the precision e = 1 / 10^t => Returns t.
        /// </summary>
        /// <returns></returns>
        public static void ComputeSystemPrecisionPower()
        {
            PrecisionPower = 1;
            Precision = 1d / 10;
            const double constant = 1.0;

            while (Math.Abs(constant + Precision - constant) > 0)
            {
                PrecisionPower++;
                Precision /= 10;
            }
            PrecisionPower--;
            Precision *= 10;
        }

        /// <summary>
        /// Given a square matrix NxN and a method which gets elements from N, pretty-prints that matrix (pads columns).
        /// </summary>
        /// <param name="N"></param>
        /// <param name="elementGetter"></param>
        /// <returns></returns>
        public static string GetMatrixStringRepresentation(int N, Func<int, int, string> elementGetter)
        {
            var columns = new List<List<string>>();
            for (var i = 0; i < N; i++)
                columns.Add(new List<string>());

            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < N; j++)
                    columns[i].Add(elementGetter(j, i));
            }

            var pads = new List<int>();
            for (var i = 0; i < N; i++)
            {
                var padWidth = columns[i].Max(x => x.Length) + 2;
                pads.Add(padWidth);
            }

            var sb = new StringBuilder();
            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < N; j++)
                {
                    var padSize = pads[j];
                    sb.Append(string.Format("{0,-" + padSize + "}", elementGetter(i, j)));
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }
}
