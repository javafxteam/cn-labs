﻿
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Factorization;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Extensions
{
    public static class HExtensions
    {
       public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
            box.ScrollToCaret();
        }

        /// <summary>
        /// Given the input string in the form of an array sepparated by spaces, it builds the current vector from that string
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="input">Possible input: '1 2'</param>
        /// <param name="id">Represents the id of this vector, to help identify if possible exceptions.</param>
        public static void BuildFromString(this Vector<double> vec,string input, string id)
        {
            var N = vec.Count;
            var columns = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (columns.Length != N)
                throw new Exception($"The number of columns in the matrix must be {N}. Error occurred on: {id}.");

            for (var j = 0; j < columns.Length; j++)
            {
                try
                {
                    vec[j] = double.Parse(columns[j], CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    throw new Exception("You must insert double numbers.");
                }
            }
        }

        /// <summary>
        /// Given the input string in the form of an matrix sepparated by spaces + \n at end of line, it builds the current matrix from that string
        /// </summary>
        /// <param name="mat"></param>
        /// <param name="input">Possible input: '1 2\n3 4\n5 6'</param>
        public static void BuildFromString(this Matrix<double> mat, string input)
        {
            var N = mat.RowCount;
            var lines = input.Split(new char[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length != N)
                throw new Exception($"The number of lines in the matrix must be {N}.");

            for (var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                var columns = new DenseVector(N);
                columns.BuildFromString(line, (i + 1).ToString());
                mat.SetRow(i, columns);
            }
        }

        /// <summary>
        /// Prettifies the LU output (L and U) into a string.
        /// </summary>
        /// <param name="decomp"></param>
        /// <returns></returns>
        public static string ToStringer(this LU<double> decomp)
        {
            var L = decomp.L;
            var U = decomp.U;
            var lMatrix = Utils.GetMatrixStringRepresentation(L.RowCount, (i, j) => L[i, j].ToString(CultureInfo.InvariantCulture));
            var uMatrix = Utils.GetMatrixStringRepresentation(U.RowCount, (i, j) => U[i, j].ToString(CultureInfo.InvariantCulture));
            return "The L Matrix:\n" + lMatrix + "\n\n" +
                   "The U Matrix:\n" + uMatrix + "\n\n";
        }
    }
}
