﻿

using System.Globalization;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Ldl
    {
        public int N;
        public Matrix<double> A;
        public Vector<double> D;

        public Ldl(int n, Matrix<double> a, Vector<double> d)
        {
            N = n;
            A = a;
            D = d;
        }

        /// <summary>
        /// det(A) = det(L) * det(D) * det(Lt). But det(L) = det(Lt) = 1 (triangular matrix => product of diagonal elements = 1). det(A) = det(D) = product of elements from D.
        /// </summary>
        public double GetDeterminant()
        {
            var detD = 1d;
            D.AsArray().ToList().ForEach(d => detD *= d);
            return detD;
        }

        public string GetDRepresentation()
        {
            return Utils.GetMatrixStringRepresentation(N, (i, j) => i == j ? D[i].ToString(CultureInfo.InvariantCulture) : "0");
        }

        public string GetLRepresentation()
        {
            return Utils.GetMatrixStringRepresentation(N, (i, j) => Utils.GetFromL(A, i + 1, j + 1).ToString(CultureInfo.InvariantCulture));
        }

        public string GetLtRepresentation()
        {
            return Utils.GetMatrixStringRepresentation(N, (i, j) => Utils.GetFromLt(A, i + 1, j + 1).ToString(CultureInfo.InvariantCulture));
        }

        public override string ToString()
        {
            return "The L Matrix:\n" + GetLRepresentation() + "\n" +
                   "The D Matrix:\n" + GetDRepresentation() + "\n" +
                   "The L™ Matrix:\n" + GetLtRepresentation() + "\n";
        }
    }
}
