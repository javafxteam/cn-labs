﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Solution
    {
        private ISolver _solver;
        private Vector<double> X;

        public Solution(ISolver solver)
        {
            _solver = solver;
            X = _solver.Solve();
        }

        public bool IsSolutionOk(out IEnumerable<double> diffs, out double norm)
        {
            var N = _solver.N;
            var Y = new DenseVector(N);
            int i;
            for (i = 1; i <= N; i++)
            {
                Y[i - 1] = 0d;
                for (var j = 1; j <= N; j++)
                    Y[i - 1] += Utils.GetFromA(_solver.A, i, j) * X[j - 1];

            }
            var temp = new List<double>();
            for(i = 1; i <= N; i ++)
                temp.Add(Y[i - 1] - _solver.B[i - 1]);
            diffs = temp;

            norm = Utils.GetNorm(diffs);
            return !(norm >= Math.Pow(10, -8));
        }

        public override string ToString()
        {
            IEnumerable<double> diffs;
            double norm;
            var isOk = IsSolutionOk(out diffs, out norm);
            var diffstring = "Differences vector: " + string.Join(" ", diffs.Select(o => o.ToString(CultureInfo.InvariantCulture))) + "\n";
            var text = isOk ? $"Solution is OK (norm < 10^-8).\n" : $"Solution is not OK (norm >= 10^-8).\n";
            var normstring = "Norm: " + norm.ToString(CultureInfo.InvariantCulture) + "\n";
            return "X: (" + string.Join(", ", X.ToArray()) + ")\n" +
                text + diffstring + normstring;
        }
    }
}
