﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using SeeSharpImplementation.Logic;
using SeeSharpImplementation.Extensions;

namespace SeeSharpImplementation
{
    public partial class MainWindow : Form
    {
        private Controller _controller;
        private Color LOG_TEXT = Color.Gray;
        private Color IMPORTANT_TEXT = Color.Crimson;
        private Color NICE_TEXT = Color.DarkGreen;
        

        public MainWindow()
        {
            InitializeComponent();
            SetUpMiddleScreenMiddleSize();
        }

        private void SetUpMiddleScreenMiddleSize()
        {
            var cScreen = Screen.FromControl(this);
            //var newSize = new Size(cScreen.Bounds.Width / 2, cScreen.Bounds.Height / 2);
            var newLocation = new Point((int)(1.0 / 4 * cScreen.Bounds.Width), (int)(1.0 / 4 * cScreen.Bounds.Height));
            //Size = newSize;
            Location = newLocation;

            Utils.ComputeSystemPrecisionPower();
            logTB.AppendText($"Precision of the system computed. It's {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n", LOG_TEXT);
            precisionUpDown.Maximum = Utils.PrecisionPower;
            precisionUpDown.Value = Utils.PrecisionPower;
            precisionLabel.Text = $"Precision (sys max: {Utils.PrecisionPower}).";
            logTB.Font = new Font(FontFamily.GenericMonospace, logTB.Font.Size);
        }


        private void computeBtn_Click(object sender, EventArgs e)
        {
            var N = (int)nUpDown.Value;
            var bString = bTB.Text;
            var aString = aTB.Text;
            try
            {
                _controller = new Controller(N, aString, bString);

                // part 1 - decomposition LDL
                var repr = _controller.DecomposeLdlAndReturnRepresentation();
                logTB.AppendText("\nComputed the LDL Decomposition.\n", LOG_TEXT);
                logTB.AppendText(repr, NICE_TEXT);

                // part 2 - determinant
                repr = _controller.GetLdlDeterminant();
                logTB.AppendText($"The Determinant: {repr}.\n\n", IMPORTANT_TEXT);

                // part 3 - solve system
                repr = _controller.GetSolutionLdl();
                logTB.AppendText($"The Ax = b (LDL™x = b) system solution:\n{repr}\n", IMPORTANT_TEXT);
                
                // part 4 - decomposition LU
                repr = _controller.DecomposeLuAndReturnRepresentation();
                logTB.AppendText("\nComputed the LU Decomposition.\n", LOG_TEXT);
                logTB.AppendText(repr, NICE_TEXT);
                
                // part 5 - solve system LU
                repr = _controller.GetSolutionLu();
                logTB.AppendText($"The Ax = b (LUx = b) system solution:\n{repr}\n", IMPORTANT_TEXT);

            }
            catch (Exception exception)
            {
                MessageBox.Show(this, exception.Message, "Exception happened.", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void precisionUpDown_ValueChanged(object sender, EventArgs e)
        {
            Utils.PrecisionPower = (int)precisionUpDown.Value;
            Utils.Precision = Math.Pow(10, -Utils.PrecisionPower);
            logTB.AppendText($"Precision set to {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n");
        }
    }
}
