﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SeeSharpImplementation.Logic;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Properties;

namespace SeeSharpImplementation
{
    public partial class MainWindow : Form
    {
        #region Fields
        private Controller _controller;
        private Backgrounder _executer;
        private int _step;

        private Color LOG_TEXT = Color.Gray;
        private Color IMPORTANT_TEXT = Color.Crimson;
        private Color NICE_TEXT = Color.DarkGreen;
        #endregion

        #region Construction
        public MainWindow()
        {
            _executer = new Backgrounder(_workProgress, logTB);
            _controller = new Controller(_executer);

            InitializeComponent();
            SetUpMiddleScreenMiddleSize();
        }

        private void SetUpMiddleScreenMiddleSize()
        {
            var cScreen = Screen.FromControl(this);
            //var newSize = new Size(cScreen.Bounds.Width / 2, cScreen.Bounds.Height / 2);
            var newLocation = new Point((int)(1.0 / 4 * cScreen.Bounds.Width), (int)(1.0 / 4 * cScreen.Bounds.Height));
            //Size = newSize;
            Location = newLocation;

            Utils.ComputeSystemPrecisionPower();
            logTB.AppendText($"Precision of the system computed. It's {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n", LOG_TEXT);
            precisionUpDown.Maximum = Utils.PrecisionPower;
            precisionUpDown.Value = 8;
            precisionLabel.Text = $@"Precision (sys max: {Utils.PrecisionPower}).";
            logTB.Font = new Font(FontFamily.GenericMonospace, logTB.Font.Size);
            
        }

        private void precisionUpDown_ValueChanged(object sender, EventArgs e)
        {
            Utils.PrecisionPower = (int)precisionUpDown.Value;
            Utils.Precision = Math.Pow(10, -Utils.PrecisionPower);
            logTB.AppendText($"Precision set to {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n");
        }

        #endregion


        #region File Loader
        private void _clearItemsBtn_Click(object sender, EventArgs e)
        {
            _loadedItemsTB.DataSource = null;
            logTB.AppendText($"Items cleared.\n", LOG_TEXT);
        }

        private void _loadItemsBTN_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = true,
                Filter = Resources.res_MainWindowOFDFilter,
                InitialDirectory = Application.StartupPath
            };
            var result = ofd.ShowDialog();
            if (result != DialogResult.OK) return;

            var files = ofd.FileNames;
            var realFiles = files.Select(file => new FileInfo(file)).ToList();

            _loadedItemsTB.DataSource = realFiles;
            _loadedItemsTB.DisplayMember = "Name";

            logTB.AppendText($"{realFiles.Count} items loaded.\n", LOG_TEXT);
        }

        private void _loadFromFileBtn_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDataInMemory();
            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }

        private void LoadDataInMemory()
        {
            if (!(_loadedItemsTB.SelectedItem is FileInfo))
                throw new Exception("Please load and select an item (polynomial file) first, kind human.");

            _laguerreBtn.Enabled = false;
            _executer = new Backgrounder(_workProgress, logTB, _loadFromFileBtn, true, _loadFromFileBtn.Text);
            _controller.SetExecuter(_executer);

            var fileInfo = (FileInfo) _loadedItemsTB.SelectedItem;
            var filePath = fileInfo.FullName;

            logTB.AppendText($"\n\nReading items from {fileInfo.Name}...\n", LOG_TEXT);


            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.ReadPolynomialFromFile(filePath);
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("The polynomial was read.\n", LOG_TEXT);
                logTB.AppendText($"\nThe polynomial:\n{_controller.P}\n", NICE_TEXT);

                _step = 0;
                _laguerreBtn.Enabled = true;
                _laguerreBtn.Text = "Run Laguerre on Loaded";
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }
        #endregion

        #region Laguerre
        
        private void _laguerreBtn_Click(object sender, EventArgs e)
        {
            try
            {
                switch (_step)
                {
                    case 0:
                        // Step 1 - Start Running Laguerre Search
                        RunLaguerre();
                        _step++;
                        break;
                    case 1:
                        // Step 1.5 - Stop Searching for solutions with Laguerre if it takes too long
                        _controller.StopFindingSolution = true;
                        break;
                }
            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }

        private void RunLaguerre()
        {
            if(_controller.P == null)
                throw  new Exception("You must first load a polynomial from a file, sir!");

            // Step 0.1 - Need to init executer for this button
            _executer = new Backgrounder(_workProgress, logTB, _laguerreBtn, false, _laguerreBtn.Text);
            _controller.SetExecuter(_executer);
            _laguerreBtn.Text = "Stop Searching!";

            logTB.AppendText($"\n\nRunning Laguerre...\n", LOG_TEXT);


            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.ComputeSolutionsForPolynomial();
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Finished.\n", LOG_TEXT);
                logTB.AppendText($"Finished running Laguerre.\n{_controller.Laguerre}\n", NICE_TEXT);
                Utils.WriteToFile("laguerre_results.txt", _controller.Laguerre.ToString(), true);
                _step = 0;//make sure it falls to the right step
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }

        #endregion


        #region Function Min

        private void _computeBtn_Click(object sender, EventArgs e)
        {
            try
            {
                RunFunctionMethod();
            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }
        private void RunFunctionMethod()
        {
            if (!(_loadedItemsTB.SelectedItem is FileInfo))
                throw new Exception("Please load and select an item (function file) first, kind human.");

            var fileInfo = (FileInfo)_loadedItemsTB.SelectedItem;
            var filePath = fileInfo.FullName;
            _controller.TryReadingFunctionFromFile(filePath);

            // Step 0.1 - Need to init executer for this button
            _executer = new Backgrounder(_workProgress, logTB, _computeBtn, true, _computeBtn.Text);
            _controller.SetExecuter(_executer);

            logTB.AppendText($"\n\nRunning Function Method...\n", LOG_TEXT);


            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.ComputeMinOfFunction();
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Finished.\n", LOG_TEXT);
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }
        #endregion
    }
}
