﻿namespace SeeSharpImplementation
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._loadFromFileBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._workProgress = new System.Windows.Forms.ProgressBar();
            this.logTB = new System.Windows.Forms.RichTextBox();
            this._laguerreBtn = new System.Windows.Forms.Button();
            this._computeBtn = new System.Windows.Forms.Button();
            this.precisionLabel = new System.Windows.Forms.Label();
            this.precisionUpDown = new System.Windows.Forms.NumericUpDown();
            this._loadedItemsTB = new System.Windows.Forms.ListBox();
            this._loadItemsBTN = new System.Windows.Forms.Button();
            this._clearItemsBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.precisionUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _loadFromFileBtn
            // 
            this._loadFromFileBtn.Location = new System.Drawing.Point(23, 419);
            this._loadFromFileBtn.Margin = new System.Windows.Forms.Padding(4);
            this._loadFromFileBtn.Name = "_loadFromFileBtn";
            this._loadFromFileBtn.Size = new System.Drawing.Size(191, 38);
            this._loadFromFileBtn.TabIndex = 1;
            this._loadFromFileBtn.Text = "Load from selected File";
            this._loadFromFileBtn.UseVisualStyleBackColor = true;
            this._loadFromFileBtn.Click += new System.EventHandler(this._loadFromFileBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._workProgress);
            this.groupBox2.Controls.Add(this.logTB);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(443, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(631, 486);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output Data";
            // 
            // _workProgress
            // 
            this._workProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._workProgress.Location = new System.Drawing.Point(8, 450);
            this._workProgress.Margin = new System.Windows.Forms.Padding(4);
            this._workProgress.Name = "_workProgress";
            this._workProgress.Size = new System.Drawing.Size(615, 28);
            this._workProgress.TabIndex = 1;
            // 
            // logTB
            // 
            this.logTB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logTB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logTB.Location = new System.Drawing.Point(8, 32);
            this.logTB.Margin = new System.Windows.Forms.Padding(4);
            this.logTB.Name = "logTB";
            this.logTB.Size = new System.Drawing.Size(613, 411);
            this.logTB.TabIndex = 0;
            this.logTB.Text = "";
            this.logTB.WordWrap = false;
            // 
            // _laguerreBtn
            // 
            this._laguerreBtn.Location = new System.Drawing.Point(222, 419);
            this._laguerreBtn.Margin = new System.Windows.Forms.Padding(4);
            this._laguerreBtn.Name = "_laguerreBtn";
            this._laguerreBtn.Size = new System.Drawing.Size(203, 38);
            this._laguerreBtn.TabIndex = 5;
            this._laguerreBtn.Text = "Run Laguerre on Loaded";
            this._laguerreBtn.UseVisualStyleBackColor = true;
            this._laguerreBtn.Click += new System.EventHandler(this._laguerreBtn_Click);
            // 
            // _computeBtn
            // 
            this._computeBtn.Location = new System.Drawing.Point(23, 463);
            this._computeBtn.Margin = new System.Windows.Forms.Padding(4);
            this._computeBtn.Name = "_computeBtn";
            this._computeBtn.Size = new System.Drawing.Size(402, 38);
            this._computeBtn.TabIndex = 6;
            this._computeBtn.Text = "Find selected Function\'s Min (Aproximated)";
            this._computeBtn.UseVisualStyleBackColor = true;
            this._computeBtn.Click += new System.EventHandler(this._computeBtn_Click);
            // 
            // precisionLabel
            // 
            this.precisionLabel.AutoSize = true;
            this.precisionLabel.Location = new System.Drawing.Point(8, 33);
            this.precisionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.precisionLabel.Name = "precisionLabel";
            this.precisionLabel.Size = new System.Drawing.Size(128, 15);
            this.precisionLabel.TabIndex = 0;
            this.precisionLabel.Text = "Precision (sys max: 16):";
            // 
            // precisionUpDown
            // 
            this.precisionUpDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.precisionUpDown.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.precisionUpDown.Location = new System.Drawing.Point(8, 62);
            this.precisionUpDown.Margin = new System.Windows.Forms.Padding(4);
            this.precisionUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.precisionUpDown.Name = "precisionUpDown";
            this.precisionUpDown.Size = new System.Drawing.Size(171, 23);
            this.precisionUpDown.TabIndex = 3;
            this.precisionUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.precisionUpDown.ValueChanged += new System.EventHandler(this.precisionUpDown_ValueChanged);
            // 
            // _loadedItemsTB
            // 
            this._loadedItemsTB.FormattingEnabled = true;
            this._loadedItemsTB.ItemHeight = 15;
            this._loadedItemsTB.Location = new System.Drawing.Point(8, 137);
            this._loadedItemsTB.Margin = new System.Windows.Forms.Padding(4);
            this._loadedItemsTB.Name = "_loadedItemsTB";
            this._loadedItemsTB.Size = new System.Drawing.Size(401, 244);
            this._loadedItemsTB.TabIndex = 10;
            // 
            // _loadItemsBTN
            // 
            this._loadItemsBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._loadItemsBTN.Location = new System.Drawing.Point(7, 97);
            this._loadItemsBTN.Margin = new System.Windows.Forms.Padding(4);
            this._loadItemsBTN.Name = "_loadItemsBTN";
            this._loadItemsBTN.Size = new System.Drawing.Size(254, 38);
            this._loadItemsBTN.TabIndex = 5;
            this._loadItemsBTN.Text = "Add File(s)";
            this._loadItemsBTN.UseVisualStyleBackColor = true;
            this._loadItemsBTN.Click += new System.EventHandler(this._loadItemsBTN_Click);
            // 
            // _clearItemsBtn
            // 
            this._clearItemsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._clearItemsBtn.Location = new System.Drawing.Point(269, 97);
            this._clearItemsBtn.Margin = new System.Windows.Forms.Padding(4);
            this._clearItemsBtn.Name = "_clearItemsBtn";
            this._clearItemsBtn.Size = new System.Drawing.Size(140, 38);
            this._clearItemsBtn.TabIndex = 11;
            this._clearItemsBtn.Text = "Clear";
            this._clearItemsBtn.UseVisualStyleBackColor = true;
            this._clearItemsBtn.Click += new System.EventHandler(this._clearItemsBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._clearItemsBtn);
            this.groupBox1.Controls.Add(this._loadItemsBTN);
            this.groupBox1.Controls.Add(this._loadedItemsTB);
            this.groupBox1.Controls.Add(this.precisionUpDown);
            this.groupBox1.Controls.Add(this.precisionLabel);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(419, 397);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input Data";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1092, 518);
            this.Controls.Add(this._computeBtn);
            this.Controls.Add(this._laguerreBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._loadFromFileBtn);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1107, 554);
            this.Name = "MainWindow";
            this.Text = "Matrixes Application";
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.precisionUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button _loadFromFileBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox logTB;
        private System.Windows.Forms.ProgressBar _workProgress;
        private System.Windows.Forms.Button _laguerreBtn;
        private System.Windows.Forms.Button _computeBtn;
        private System.Windows.Forms.Label precisionLabel;
        private System.Windows.Forms.NumericUpDown precisionUpDown;
        private System.Windows.Forms.ListBox _loadedItemsTB;
        private System.Windows.Forms.Button _loadItemsBTN;
        private System.Windows.Forms.Button _clearItemsBtn;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

