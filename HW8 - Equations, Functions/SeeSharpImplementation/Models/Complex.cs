﻿using System;
using System.Globalization;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Complex
    {
        public double R { get; set; }
        public double I { get; set; }

        public Complex()
        {
            R = I = 0;
        }

        public Complex(double r, double i)
        {
            R = r;
            I = i;
        }

		public Complex Conjugate => new Complex(R, -I);

		public Complex Negated => new Complex(-R, -I);

		public double SquareOfIndices => R * R + I * I;

        public double Abs => Math.Sqrt(SquareOfIndices);

		/// <summary>
        /// Uses the csgn function
        /// </summary>
        public double Sign => R > 0 ? 1 : (R < 0 ? -1 : (I >= 0 ? 1 : -1));

        public Complex Sqrt => Math.Sqrt(Abs) * (Abs + this) / (Abs + this).Abs;

        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex
            {
                R = c1.R + c2.R,
                I = c1.I + c2.I
            };
        }

        public static Complex operator +(double n, Complex c)
        {
            return new Complex
            {
                R = n + c.R,
                I = c.I
            };
        }

        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex
            {
                R = c1.R - c2.R,
                I = c1.I - c2.I
            };
        }

        public static Complex operator *(Complex c1, Complex c2)
        {
            return new Complex
            {
                R = c1.R * c2.R - c1.I * c2.I,
                I = c1.R * c2.I + c1.I * c2.R
            };
        }

        public static Complex operator *(double n, Complex c)
        {
            return new Complex
            {
                R = n * c.R,
                I = n * c.I
            };
        }

        public static Complex operator /(Complex c1, Complex c2)
        {
            return c1 * c2.Conjugate / (c2.SquareOfIndices);
        }

        public static Complex operator /(Complex c, double n)
        {
            return Math.Abs(n) > Utils.Precision ? ((1/n) * c) : (0 * c);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Complex))
                return false;

            var c = (Complex) obj;
            return Math.Abs(R - c.R) < Utils.Precision && Math.Abs(I - c.I) < Utils.Precision;
        }

        protected bool Equals(Complex other)
        {
            return R.Equals(other.R) && I.Equals(other.I);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (R.GetHashCode() * 397) ^ I.GetHashCode();
            }
        }

        public override string ToString()
        {
            var output = string.Empty;
            if (Math.Abs(R) > Utils.Precision)
                output += R.ToString(CultureInfo.InvariantCulture);
            if (Math.Abs(I) > Utils.Precision)
                output += (I > 0 ? " + " : " - ") + "i * " + Math.Abs(I).ToString(CultureInfo.InvariantCulture);
            return output;
        }
    }
}
