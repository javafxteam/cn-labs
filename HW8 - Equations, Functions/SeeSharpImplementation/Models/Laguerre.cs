﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Laguerre
    {
        public Polynomial P { get; set; }
        public List<double> Solutions { get; set; }
        public List<Complex> ComplexSolutions { get; set; }
        public int SolutionsFound => Solutions.Count + ComplexSolutions.Count;

        public Laguerre()
        {
            Solutions = new List<double>();
            ComplexSolutions = new List<Complex>();
        }

        public bool AddSolution(double sol)
        {
            if (!(SolutionsFound <= P.N - 1))//skip fault adding
                return false;

            if (Solutions.Any(solution => Math.Abs(solution - sol) <= Utils.Precision))
            {
                return false;
            }
            if (Math.Abs(P[sol]) > Utils.Precision)
                return false;

            Solutions.Add(sol);
            return true;
        }
        public bool AddSolution(Complex sol)
        {
            if (Math.Abs(sol.I) <= Utils.Precision) //found double solution
            {
                return AddSolution(sol.R);
            }

            if (!(SolutionsFound <= P.N - 2))
                return false;

            if (ComplexSolutions.Contains(sol))
            {
                return false;
            }

            if (P[sol].Abs > Utils.Precision)
                return false;

            ComplexSolutions.Add(sol);
            ComplexSolutions.Add(sol.Conjugate);// the conjugate is also a solution
            return true;
        }

        public override string ToString()
        {
            var realSolutions = string.Join(", ", Solutions.Select(x => x.ToString(CultureInfo.InvariantCulture)));
            var complexSolutions = string.Join(", ", ComplexSolutions.Select(x => x.ToString()));
            var solutionsString = realSolutions + (Solutions.Count != 0 && ComplexSolutions.Count != 0 ? ", " : "") +
                                  complexSolutions;
            return $"Polynomial is {P}.\r\n" +
                   $"Solutions found in [-{P.R}, {P.R}].\r\n" +
                   $"Solutions({SolutionsFound}): [{solutionsString}]\r\n\r\n";
        }
    }
}
