﻿using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using org.mariuszgromada.math.mxparser;

namespace SeeSharpImplementation.Models
{
    public class Function
    {
        #region Fields
        public static double H = Math.Pow(10, -6);
        public static double R = Math.Pow(10, 5);
        public static double Ri = Math.Pow(10, -5);

        private int _derivativeMode;
        public int DerivativeMode
        {
            get
            {
                return _derivativeMode;
            }
            set
            {
                _derivativeMode = value;
                _usedStartPoints = new SortedSet<double>();
            }
        }

        public Function F => this;
        public bool IsValid => _expression.checkSyntax();

        private readonly Expression _expression;
        private readonly Random _random;
        private SortedSet<double> _usedStartPoints;
        #endregion


        public Function(string functionString, string parameter = "x")
        {
            _usedStartPoints = new SortedSet<double>();
            _random = new Random();
            DerivativeMode = 1;
            
            var x = new Argument(parameter + " = 0");
            _expression = new Expression(functionString, x);
            
        }

        #region Overrides

        public double this[double x]
        {
            get
            {
                _expression.getArgument(0).setArgumentValue(x);
                return _expression.calculate();
            }
        }

        public override string ToString()
        {
            return _expression.getExpressionString();
        }
        #endregion

        #region Mathematical Aproximations

        public double Derivative(double x)
        {
            return DerivativeMode == 1 ? DerivativeG1(x) : DerivativeG2(x);
        }

        public double DerivativeG1(double x)
        {
            return (3 * F[x] - 4 * F[x - H] + F[x - 2 * H]) / (2 * H);
        }

        public double DerivativeG2(double x)
        {
            return (-F[x + 2 * H] + 8 * F[x + H] - 8 * F[x - H] + F[x - 2* H]) / (12 * H);
        }

        public double Derivative2Nd(double x)
        {
            return (- F[x + 2 * H] + 16 * F[x + H] - 30 * F[x] + 16 * F[x - H] - F[x - 2 * H]) / (12 * H * H);
        }
        #endregion
        
        
        /// <summary>
        /// idea - start from points with F''(x) > 0, unused
        /// </summary>
        private double GetRandomValidStartingPoint()
        {
            double x;
            
            while (true)
            {
                x = _random.GetRandomDouble(-R, R, true);

                if (Derivative2Nd(x) <= 0)// keep searching until I find one with F''(x) > 0
                    continue;

                if (!_usedStartPoints.Any(oldx => Math.Abs(oldx - x) <= Utils.Precision))//break if start point used
                    break;

            }
            _usedStartPoints.Add(x);
            return x;
        }

        public double? GetMinPoint()
        {
            var x0 = GetRandomValidStartingPoint();
            var x1 = GetRandomValidStartingPoint();
            var k = 0;
            double absdeltax;

            do
            {
                var numitor = Derivative(x1) - Derivative(x0);
                numitor = Math.Abs(numitor) < Utils.Precision ? Ri : numitor;
                var deltax = (x1 - x0) * Derivative(x1) / numitor;
                absdeltax = Math.Abs(deltax);
                x0 = x1;
                x1 = x1 - deltax;
                k++;

            } while (absdeltax >= Utils.Precision && k < Utils.MaxNumberOfStepsForFunctionMethod && absdeltax <= Utils.MaxValueForDeltaX);

            if (absdeltax < Utils.Precision)
                return x1;

            return null;//divergenta
        }
    }
}
