﻿
using System;
using System.Collections.Generic;
using System.Linq;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Polynomial
    {
        public int N { get; set; }
        public double X { get; set; }
        public double StartingX { get; set; }
        public Complex StartingComplexX { get; set; }
        public double[] Coefficients { get; set; }
        public int[] Powers { get; set; }

        private Random _random;
        private SortedSet<double> _usedStartPoints;
        private HashSet<Complex> _usedComplexStartPoints;

        #region Constructors

        /// <summary>
        /// Inits a basic polynomial with all the coefficients set to 0
        /// </summary>
        public Polynomial(int n)
        {
            BasicInit(n);
        }

        /// <summary>
        /// Inits this polynomial with N and a list of doubles representing the ordered (a0, a1, a2...) coefficients
        /// </summary>
        public Polynomial(int n, IReadOnlyList<double> coefficients)
        {
            BasicInit(n);
            SetCoefficients(coefficients);
        }

        /// <summary>
        /// Inits this polynomial with N and a list of key-value pairs representing coefficients; the values represent the coefficient's value and the key represents the coefficient's position [0 - N] (weather it's a0, a1, a2 ...)
        /// </summary>
        public Polynomial(int n, Dictionary<int, double> selectiveIndexAndCoefficientPairs)
        {
            BasicInit(n);
            SetCoefficients(selectiveIndexAndCoefficientPairs);
        }

        public void SetCoefficients(IReadOnlyList<double> coefficients)
        {
            for (var i = 0; i <= N; i++)
                Coefficients[i] = i < coefficients.Count ? coefficients[i] : 0;
        }

        public void SetCoefficients(Dictionary<int, double> selectiveIndexAndCoefficientPairs)
        {
            foreach (var pair in selectiveIndexAndCoefficientPairs)
            {
                if (pair.Key < 0 || pair.Key > N)
                    throw new ArgumentException();
                Coefficients[pair.Key] = pair.Value;
            }
        }

        private void BasicInit(int n)
        {
            N = n;
            Coefficients = new double[N + 1];

            Powers = new int[N + 1];
            for (var i = 0; i <= N; i++)
            {
                Powers[i] = N - i;
            }
            _random = new Random();
        }

        public void ResetStartPoints()
        {
            _usedStartPoints = new SortedSet<double>();
            _usedComplexStartPoints = new HashSet<Complex>();
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Given the fact X is all set up, returns the value of the element (in the polynom) of the index-th element. If you want to compute the polynom in a value, use the double overload - this one returns only the value at a given position
        /// </summary>
        public double this[int index]
        {
            get
            {
                if(index < 0 || index > N)
                    throw  new ArgumentException();
                return Coefficients[index] * Math.Pow(X, Powers[index]);//modify this to use Horner
            }
        }

        /// <summary>
        /// Computes the polynom in the given value (double).
        /// </summary>
        public double this[double value]
        {
            get
            {
                X = value;
                return Horner(value);
            }
        }

        /// <summary>
        /// Computes the polynom in the given value (Complex).
        /// </summary>
        public Complex this[Complex value] => ComputeComplex(value);

        public override string ToString()
        {
            var output = $"[N = {N}] P(x) = ";
            for (var i = 0; i <= N; i++)
            {
                if(Math.Abs(Coefficients[i]) < Utils.Precision)
                    continue;

                var coeff = Coefficients[i];
                var xval = Powers[i] == 0 ? "" : Powers[i] == 1 ? "X" : "X^" + Powers[i];

                if (i >= 1) // works because a0 != 0 always
                {
                    output += coeff >= 0 ? " + " : " - ";
                    coeff = Math.Abs(coeff);
                }


                output += coeff == 1 ? xval : coeff + xval;

            }
            if (N == 0)
                output += "0";

            return output;
        }

        #endregion

        #region Mathematical Computations

        /// <summary>
        /// Returns the R as in [-R,R] is the interval where all the solutions of this polynomial reside.
        /// </summary>
        public double R
        {
            get
            {
                if (N == 0) return 0;

                var a = Coefficients.Skip(1).Select(Math.Abs).Max();
                var absA0 = Math.Abs(Coefficients[0]);
                return (absA0 + a) / absA0;
            }
        }

        /// <summary>
        /// Computes and returns this Polynomial's Derivative, as a brand new Polynomial
        /// </summary>
        public Polynomial Derivative
        {
            get
            {
                if (N == 0 || N - 1 == 0)
                {
                    return new Polynomial(0);
                }

                var newP = new Polynomial(N - 1);
                // set coefficients to ai * (n - i) (the last one disappears)
                for (var i = 0; i < N; i++)
                    newP.Coefficients[i] = Coefficients[i] * Powers[i];

                // set powers (decrement)
                for (var i = 0; i < N; i++)
                    newP.Powers[i] = N - i - 1;

                return newP;

            }
        }

        /// <summary>
        /// Computes the Polynomial's value in V, using the Horner Scheme.
        /// Reduces the polynomial to: (....(((a0X + a1)X + a2)X+.....)X + an
        /// </summary>
        public double Horner(double v)
        {
            var b = Coefficients[0];
            for (var i = 1; i <= N; i++)
                b = Coefficients[i] + b * v;

            return b;
        }

        /// <summary>
        /// Computes the value of a polynomial in a complex number (uses polynomial division).
        /// </summary>
        public Complex ComputeComplex(Complex v)
        {
            var p = -2 * v.R;
            var q = Math.Pow(v.R, 2) + Math.Pow(v.I, 2);
            var b0 = Coefficients[0];
            var b = N > 0 ? Coefficients[1] - p * b0 : -p * b0;

            for (var i = 2; i <= N; i++)
            {
                var newVal = Coefficients[i] - p * b - q * b0;
                b0 = b;
                b = newVal;
            }
            var r0 = b0;
            var r1 = b + p * b0;

            return new Complex
            {
                R = r0 * v.R + r1,
                I = r0 * v.I
            };
        }
        #endregion

        #region Laguerre for doubles
        /// <summary>
        /// // get unused start point in [-R, R] (close to possible solutions)
        /// </summary>
        private double GetRandomValidStartingPoint()
        {
            double x;
            while (true)
            {
                x = _random.GetRandomDouble(-R, R, true);
                if (!_usedStartPoints.Any(oldx => Math.Abs(oldx - x) <= Utils.Precision))
                    break;
            }
            StartingX = x;
            _usedStartPoints.Add(x);
            return x;
        }

        /// <summary>
        /// Runs the Laguerre Method for getting one solution for the Polynomial. It starts out with a random x0 in the interval [-R, R]
        /// </summary>
        public double? LaguerreForOneSolution()
        {
            var x = GetRandomValidStartingPoint();
            var k = 0;
            double absdeltax;

            do
            {
                var px = this[x];
                var derivative = Derivative[x];
                var derivative2 = Derivative.Derivative[x];

                var hx = Math.Pow(N - 1, 2) * Math.Pow(derivative, 2) - (N * (N - 1) * px * derivative2);

                var numitorDeltax = derivative + derivative.Sign() * Math.Sqrt(hx);
                var deltax = N * px / numitorDeltax;
                absdeltax = Math.Abs(deltax);

                if (deltax < 0) //restart
                    return null;

                if (Math.Abs(numitorDeltax) < Utils.Precision) //restart
                    return null;

                x = x - deltax;
                k++;

            } while (absdeltax >= Utils.Precision && k <= Utils.MaxNumberOfStepsForLaguerreMethod && absdeltax <= Utils.MaxValueForDeltaX);

            if (absdeltax < Utils.Precision)
                return x;

            return null;//divergenta
        }
        #endregion

        #region Laguerre for Complex
        /// <summary>
        /// // get unused start point in [-R, R] (close to possible solutions)
        /// </summary>
        private Complex GetRandomComplexValidStartingPoint()
        {
            Complex x;
            while (true)
            {
                var r = _random.GetRandomDouble(-R, R, true);
                var i = _random.GetRandomDouble(-R, R, true);
                x = new Complex(r, i);
                if (!_usedComplexStartPoints.Contains(x))
                    break;
            }
            StartingComplexX = x;
            _usedComplexStartPoints.Add(x);
            return x;
        }

        /// <summary>
        /// Runs the Laguerre Method for getting one solution for the Polynomial, a COMPLEX Solution. It starts out with a random x0 in the interval [-R, R]
        /// </summary>
        public Complex LaguerreForOneComplexSolution()
        {
            var x = GetRandomComplexValidStartingPoint();
            var k = 0;
            double absdeltax;

            do
            {
                var px = this[x];
                var derivative = Derivative[x];
                var derivative2 = Derivative.Derivative[x];

                var hx = Math.Pow(N - 1, 2) * (derivative * derivative) - (N * (N - 1) * px * derivative2);

                var numitorDeltax = derivative + derivative.Sign * hx.Sqrt;
                var deltax = N * px / numitorDeltax;
                absdeltax = deltax.Abs;

                //if (deltax.Sign < 0) //restart
                //    return null;

                if (numitorDeltax.Abs < Utils.Precision) //restart
                    return null;

                x = x - deltax;
                k++;

            } while (absdeltax >= Utils.Precision && k <= Utils.MaxNumberOfStepsForLaguerreMethod && absdeltax <= Utils.MaxValueForDeltaX);

            return absdeltax < Utils.Precision ? x : null;
        }
        #endregion
    }
}
