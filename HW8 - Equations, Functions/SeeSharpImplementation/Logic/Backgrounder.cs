﻿using SeeSharpImplementation.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SeeSharpImplementation.Logic
{
    public class Backgrounder
    {
        private BackgroundWorker _bw;
        private readonly ProgressBar _pb;
        private readonly RichTextBox _tb;
        private readonly Button _actionButton;
        public string NextAction;
        private DateTime _startTime, _endTime;
        private readonly bool _autoDisableButton;

        public Backgrounder(ProgressBar toUpdate, RichTextBox toLog, Button actionButton = null, bool autoDisableButton = false, string nextActionName = "")
        {
            _pb = toUpdate;
            _tb = toLog;
            _actionButton = actionButton;
            _autoDisableButton = autoDisableButton;
            NextAction = nextActionName;

            InitBw();
        }

        public void InitBw()
        {
            _bw = new BackgroundWorker
            {
                WorkerReportsProgress = true
            };

            _bw.ProgressChanged += (o, args) =>
            {
                _pb.SetProgress(args.ProgressPercentage);

            };
        }

        public void AddLogMessage(string message, Color? color = null)
        {
            _tb.AppendText(message + "\n", color ?? Color.Gray);
        }

        public void UpdateUpperBounds(int newUpperBound)
        {
            _pb.SetMaxValue(newUpperBound);
        }

        public void UpdateProgress(int percent)
        {
            _bw.ReportProgress(percent);
        }

        public void SetDoWork(Action<object, DoWorkEventArgs> actionToExecute)
        {
            _bw.DoWork += (o, args) =>
            {
                //try
                //{
                    actionToExecute(o, args);
                //}
                //catch (Exception exception)
                //{
                //    _tb.ShowWarningFromException(exception);
                //    AddLogMessage($"Operation could not complete. Reason: {exception.Message}.", Color.Red);
                //    _pb.SetProgress(0);
                //    throw new Exception(exception.Message);
                //}
            };
        }

        public void SetOnWorkerCompleted(Action<object, RunWorkerCompletedEventArgs> onActionEnd)
        {
            _bw.RunWorkerCompleted += (o, args) =>
            {
                if (args.Error != null)
                    return;

                onActionEnd(o, args);

                _endTime = DateTime.Now;
                var how = (_endTime - _startTime);
                string duration = how.ToString();
                AddLogMessage($"Operation took {duration}.");
                _pb.SetProgress(0);
                
                if(_actionButton != null)
                {
                    _actionButton.Enabled = true;
                    _actionButton.Text = NextAction;
                }

                InitBw();
            };
        }
        
        public void Start()
        {
            if (_bw == null)
                throw new Exception("Trying to run inexistent action in background.");

            if (_bw.IsBusy)
                throw new Exception("Please first wait for the background operation to end then start a new one.");

            if (_actionButton != null && _autoDisableButton)
                _actionButton.Enabled = false;

            _startTime = DateTime.Now;
            _bw.RunWorkerAsync();
        }
    }
}
