﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SeeSharpImplementation.Logic
{
    internal class Utils
    {
        public static double Precision;
        public static int PrecisionPower;
        public static double MinMaxGenerated = 6969;
        public static int MaxElementsPerLine = 10;
        public static int MaxNumberOfStepsForLaguerreMethod = 1000000;
        public static int MaxNumberOfStepsForFunctionMethod = 1000000;
        public static int MaxValueForDeltaX = 100000000;

        /// <summary>
        /// Checks if a number if zero given the error-precision to take into account.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="precision"></param>
        public static void CheckIfZero(double number, double precision)
        {
            if (Math.Abs(number) <= precision)
                throw new Exception("That number is so close to Zero, Oh Dear...");
        }

        /// <summary>
        /// Given the precision e = 1 / 10^t => Returns t.
        /// </summary>
        /// <returns></returns>
        public static void ComputeSystemPrecisionPower()
        {
            PrecisionPower = 1;
            Precision = 1d / 10;
            const double constant = 1.0;

            while (Math.Abs(constant + Precision - constant) > 0)
            {
                PrecisionPower++;
                Precision /= 10;
            }
            PrecisionPower--;
            Precision *= 10;
        }


        public static void WriteToFile(string filePath, string content, bool append = false)
        {
            using (var writer = new StreamWriter(filePath, append))
            {
                writer.WriteLine(content);
            }
        }


        /// <summary>
        /// Given a square matrix NxN and a method which gets elements from N, pretty-prints that matrix (pads columns).
        /// </summary>
        /// <param name="n"></param>
        /// <param name="elementGetter"></param>
        /// <returns></returns>
        public static string GetMatrixStringRepresentation(int m, int n, Func<int, int, string> elementGetter)
        {
            var columns = new List<List<string>>();
            for (var i = 0; i < n; i++)
                columns.Add(new List<string>());

            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < m; j++)
                    columns[i].Add(elementGetter(j, i));
            }

            var pads = new List<int>();
            for (var i = 0; i < n; i++)
            {
                var padWidth = columns[i].Max(x => x.Length) + 2;
                pads.Add(padWidth);
            }

            var sb = new StringBuilder();
            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    var padSize = pads[j];
                    sb.Append(string.Format("{0,-" + padSize + "}", elementGetter(i, j)));
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }
}
