﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class Controller
    {
        private Backgrounder _worker;//represents the worker that runs me. ref it to make him update

        public int N { get; set; }
        public Polynomial P { get; set; }
        public Function F { get; set; }
        public Laguerre Laguerre { get; set; }
        public bool StopFindingSolution { get; set; }
        

        public Controller(Backgrounder worker)
        {
            _worker = worker;
        }

        public void SetExecuter(Backgrounder worker)
        {
            _worker = worker;
        }


        /// <summary>
        /// Builds a Polynomial from a file. File has format:
        /// N
        /// 
        /// coefficientIndex1 coefficientValue1
        /// coefficientIndex2 coefficientValue2
        /// ...
        /// Indexes are the 'i' in ai, the polynomial being a0X^n + a1X^(n-1) + ..... + an.
        /// </summary>
        public void ReadPolynomialFromFile(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            {
                N = int.Parse(reader.ReadLine());
                reader.ReadLine();

                var pStr = reader.ReadToEnd();
                var lines = pStr.Split(new[] { "\n\r", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                var dict = lines.Select(line => line.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries)).ToDictionary(values => int.Parse(values[0]), values => double.Parse(values[1], CultureInfo.InvariantCulture));
                
                P = new Polynomial(N, dict);
            }
        }


        public void ComputeSolutionsForPolynomial()
        {
            Laguerre = new Laguerre
            {
                P = P
            };
            P.ResetStartPoints();

            var steps = 0;
            const int upperBound = 100;
            _worker.UpdateUpperBounds(upperBound);
            _worker.AddLogMessage($"Looking for solutions in [-{P.R}, {P.R}]...\nI'll stop when you say stop or when I find them all ({N}).\n", Color.Gray);

            StopFindingSolution = false;
            while (!StopFindingSolution && Laguerre.SolutionsFound < N)
            {
                var result = P.LaguerreForOneSolution();
                var complexResult = P.LaguerreForOneComplexSolution();
                _worker.AddLogMessage($"Looked for solutions starting from {P.StartingX} (real) and {P.StartingComplexX} (complex). ");
                _worker.UpdateProgress((steps++) % upperBound);

                if (!result.HasValue && complexResult == null)
                {
                    _worker.AddLogMessage("No results. Trying another one.\n", Color.Crimson);
                    continue;
                }

                if (complexResult != null)
                {
                    var succes = Laguerre.AddSolution(complexResult);
                    _worker.AddLogMessage($"Found {(!succes ? "already existing / invalid" : "")} solution ({complexResult}). I have {Laguerre.SolutionsFound} solutions.\n", succes ? Color.DarkGreen : Color.Gray);

                }

                if (result.HasValue)
                {
                    var succes = Laguerre.AddSolution(result.Value);
                    _worker.AddLogMessage($"Found {(!succes ? "already existing / invalid" : "")} solution ({result.Value}). I have {Laguerre.SolutionsFound} solutions.\n", succes ? Color.DarkGreen : Color.Gray);
                }
                 
            }
            _worker.AddLogMessage($"Stopped after {steps} steps.", Color.Crimson);
        }

        public void TryReadingFunctionFromFile(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            {
                reader.ReadLine();
                F = new Function(reader.ReadLine());
                
                if(!F.IsValid)
                    throw new Exception("The input function is not valid. Check it!");
            }
        }

        public void ComputeMinOfFunction()
        {
            _worker.AddLogMessage($"Aproximating Min for Function {F}.\n", Color.Crimson);

            // get solution using first F' aprox
            _worker.AddLogMessage("Running first method (G1)...");
            F.DerivativeMode = 1;
            double? sol;
            double der1, der2;
            while (true)
            {
                sol = F.GetMinPoint();
                if (!sol.HasValue) continue;

                der1 = F.Derivative(sol.Value);
                der2 = F.Derivative2Nd(sol.Value);
                if (Math.Abs(der1) < Utils.Precision && der2 > 0)
                    break;
            }
            var message = $"Found Solution x = {sol}."; 
            var messageLog = $"F'(x) ~ {der1} (distance to 0: {Math.Abs(der1)}).\n" + 
                $"F''(x) ~ {der2} ( > 0 so it should be good).\n";
            _worker.AddLogMessage(message, Color.DarkGreen);
            _worker.AddLogMessage(messageLog);

            _worker.AddLogMessage("Running second method (G2)...\n");
            F.DerivativeMode = 2;
            while (true)
            {
                sol = F.GetMinPoint();

                if (!sol.HasValue) continue;

                der1 = F.Derivative(sol.Value);
                der2 = F.Derivative2Nd(sol.Value);
                if (Math.Abs(der1) < Utils.Precision && der2 > 0)
                    break;
            }
            message = $"Found Solution x = {sol}.";
            messageLog = $"F'(x) ~ {der1} (distance to 0: {Math.Abs(der1)}).\n" +
                $"F''(x) ~ {der2} ( > 0 so it should be good).\n";
            _worker.AddLogMessage(message, Color.DarkGreen);
            _worker.AddLogMessage(messageLog);
        }
    }
}
