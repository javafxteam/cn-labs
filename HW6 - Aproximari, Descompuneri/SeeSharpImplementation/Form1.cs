﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SeeSharpImplementation.Logic;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Properties;

namespace SeeSharpImplementation
{
    public partial class MainWindow : Form
    {
        private Controller _controller;
        private Backgrounder _executer;
        private int _step;

        private Color LOG_TEXT = Color.Gray;
        private Color IMPORTANT_TEXT = Color.Crimson;
        private Color NICE_TEXT = Color.DarkGreen;
        

        public MainWindow()
        {
            _executer = new Backgrounder(_workProgress, logTB);
            _controller = new Controller(_executer);

            InitializeComponent();
            SetUpMiddleScreenMiddleSize();
        }

        private void SetUpMiddleScreenMiddleSize()
        {
            var cScreen = Screen.FromControl(this);
            //var newSize = new Size(cScreen.Bounds.Width / 2, cScreen.Bounds.Height / 2);
            var newLocation = new Point((int)(1.0 / 4 * cScreen.Bounds.Width), (int)(1.0 / 4 * cScreen.Bounds.Height));
            //Size = newSize;
            Location = newLocation;

            Utils.ComputeSystemPrecisionPower();
            logTB.AppendText($"Precision of the system computed. It's {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n", LOG_TEXT);
            precisionUpDown.Maximum = Utils.PrecisionPower;
            precisionUpDown.Value = 8;
            precisionLabel.Text = $@"Precision (sys max: {Utils.PrecisionPower}).";
            logTB.Font = new Font(FontFamily.GenericMonospace, logTB.Font.Size);

            _pEqN.Checked = true;
        }

        private string GetCurrentBtnDefaultText()
        {
            return _pEqN.Checked ? "Test for Symmetry" : "Run SVD";
        }

        private void ResetView()
        {
            _step = _pEqN.Checked ? 0 : 2;
            _computeBtn.Enabled = true;
            _computeBtn.Text = GetCurrentBtnDefaultText();
        }

        private void DisableView()
        {
            _step = _pEqN.Checked ? 0 : 2;
            _controller.IsPeqToN = _pEqN.Checked;
            _sValue.Enabled = !_controller.IsPeqToN;
            _computeBtn.Enabled = false;
            _computeBtn.Text = GetCurrentBtnDefaultText();
            // log message
            var message = _controller.IsPeqToN ? "\nWorking with Rare Matrices NxN." : "\nWorking with Classic Matrices NxN.";
            logTB.AppendText(message, LOG_TEXT);
        }

        private void precisionUpDown_ValueChanged(object sender, EventArgs e)
        {
            Utils.PrecisionPower = (int)precisionUpDown.Value;
            Utils.Precision = Math.Pow(10, -Utils.PrecisionPower);
            logTB.AppendText($"Precision set to {Utils.Precision} (10^(-{Utils.PrecisionPower})).\n");
        }
        
        private void _pEqN_CheckedChanged(object sender, EventArgs e)
        {
            DisableView();
        }

        private void _clearItemsBtn_Click(object sender, EventArgs e)
        {
            _loadedItemsTB.DataSource = null;
            logTB.AppendText($"Items cleared.\n", LOG_TEXT);
        }

        private void _loadItemsBTN_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = true,
                Filter = Resources.res_MainWindowOFDFilter,
                InitialDirectory = Application.StartupPath
            };
            var result = ofd.ShowDialog();
            if (result != DialogResult.OK) return;

            var files = ofd.FileNames;
            var realFiles = files.Select(file => new FileInfo(file)).ToList();

            _loadedItemsTB.DataSource = realFiles;
            _loadedItemsTB.DisplayMember = "Name";

            logTB.AppendText($"{realFiles.Count} items loaded.\n", LOG_TEXT);
        }


        #region Make Random Matrix in Controller
        private void _makeRandomBtn_Click(object sender, EventArgs e)
        {
            try
            {
                MakeRandomMatrix();
            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }

        public void VerifyP()
        {
            try
            {
                _controller.P = int.Parse(_pValue.Text);
            }
            catch
            {
                throw new Exception("The value for P must be an integer.");
            }

            if (_controller.IsPeqToN && _controller.P <= 500)
                throw new Exception("P must be bigger than 500.");
        }

        private void MakeRandomMatrix()
        {
            VerifyP();

            _computeBtn.Enabled = false;
            _executer = new Backgrounder(_workProgress, logTB, _makeRandomBtn, "Make Random");
            _controller.SetExecuter(_executer);


            logTB.AppendText($"\n\nGenerating random matrix {_controller.P}x{_controller.P}...\n", LOG_TEXT);
            

            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.MakeRandomMatrix();
                Utils.WriteToFile("log.txt", _controller.GetMatrixFullString());
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Random matrix created.\n", LOG_TEXT);
                logTB.AppendText($"Matrix:\n{_controller.GetMatrixString()}\n", NICE_TEXT);
                ResetView();
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }
        #endregion



        #region Load Matrix from file in Controller
        private void _loadFromFileBtn_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDataInMemory();
            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }

        private void LoadDataInMemory()
        {
            if (_loadedItemsTB.SelectedItem == null || (_loadedItemsTB.SelectedItem as FileInfo) == null)
                throw new Exception("Please load and select an item (matrix) first, kind human.");

            _computeBtn.Enabled = false;
            _executer = new Backgrounder(_workProgress, logTB, _loadFromFileBtn, "Load from File");
            _controller.SetExecuter(_executer);

            var fileInfo = (FileInfo) _loadedItemsTB.SelectedItem;
            var filePath = fileInfo.FullName;

            logTB.AppendText($"\n\nReading items from {fileInfo.Name}...\n", LOG_TEXT);


            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.ReadMatrixFromFile(filePath);
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Data loaded in memory.\n", LOG_TEXT);
                logTB.AppendText($"Matrix:\n{_controller.GetMatrixString()}\n", NICE_TEXT);
                ResetView();
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }
        #endregion



        private void _computeBtn_Click(object sender, EventArgs e)
        {
            try
            {
                switch (_step)
                {
                    case 0:
                        // Step 0.1 - Need to init executer for this button
                        _executer = new Backgrounder(_workProgress, logTB, _computeBtn, "Test for Symmetry");
                        _controller.SetExecuter(_executer);

                        // Step 1 - Metoda Puterii prin aproximare (first sym test)
                        _executer.NextAction = "Pow Method";
                        TestForSymmetry();
                        _step++;
                        break;
                    case 1:
                        _executer.NextAction = "Run again";
                        PowMethod();
                        break;
                    case 2:
                        // P > N - use SVD for reading normal matrixes
                        _executer = new Backgrounder(_workProgress, logTB, _computeBtn, "Run SVD");
                        _controller.SetExecuter(_executer);

                        _executer.NextAction = "Run again";
                        SvdMethod();
                        break;
                }

            }
            catch (Exception exception)
            {
                logTB.AppendText(Resources.res_ExceptionHappened + " ", LOG_TEXT);
                logTB.AppendText(exception.Message, IMPORTANT_TEXT);
                logTB.ShowWarningFromException(exception);
            }
        }

        private void TestForSymmetry()
        {
            logTB.AppendText($"\n\nTesting file matrix for Symmetry ...\n", LOG_TEXT);

            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.TestSymmetry();
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Finished.", LOG_TEXT);
                var symString = _controller.GetSymmetricMatrixString();
                logTB.AppendText($"\n{symString}\n", symString.Contains("NOT")? IMPORTANT_TEXT : NICE_TEXT);
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }

        private void PowMethod()
        {
            logTB.AppendText($"\n\nRunning Pow Method ...\n", LOG_TEXT);

            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.RunPowMethod();
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Finished.", LOG_TEXT);

                logTB.AppendText($"\n{_controller.GetPowMethodResultMessage()}\n", _controller.IsPowSolutionOk ? NICE_TEXT : IMPORTANT_TEXT);
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }

        private void SvdMethod()
        {

            try
            {
                _controller.S = int.Parse(_sValue.Text);
            }
            catch
            {
                throw new Exception("The value for S must be an integer.");
            }

            logTB.AppendText($"\n\nRunning SVD Method ...\n", LOG_TEXT);

            Action<object, DoWorkEventArgs> action = (o, args) =>
            {
                _controller.RunSvdMethod();
            };
            _executer.SetDoWork(action);

            Action<object, RunWorkerCompletedEventArgs> onCompleted = (o, args) =>
            {
                logTB.AppendText("Finished.", LOG_TEXT);

                logTB.AppendText($"\n{_controller.GetSvdData()}\n", IMPORTANT_TEXT);
            };
            _executer.SetOnWorkerCompleted(onCompleted);

            _executer.Start();
        }
    }
}
