﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SeeSharpImplementation.Properties;

namespace SeeSharpImplementation.Extensions
{
    public static class VisualExtensions
    {
        // thread safe append text
       public static void AppendText(this RichTextBox box, string text, Color color)
        {
            if (box.InvokeRequired)
            {
                box.Invoke(new Action<RichTextBox, string, Color>(AppendText), box, text, color);
            }
            else
            {
                box.SelectionStart = box.TextLength;
                box.SelectionLength = 0;

                box.SelectionColor = color;
                box.AppendText(text);
                box.SelectionColor = box.ForeColor;
                box.ScrollToCaret();
            }
        }

        // thread safe SetProgress
        public static void SetProgress(this ProgressBar pb, int value)
        {
            if(pb.InvokeRequired)
            {
                pb.Invoke(new Action<ProgressBar, int>(SetProgress), pb, value);
            }
            else
            {
                pb.Value = value;
            }
        }

        // thread safe SetMax
        public static void SetMaxValue(this ProgressBar pb, int value)
        {
            if (pb.InvokeRequired)
            {
                pb.Invoke(new Action<ProgressBar, int>(SetMaxValue), pb, value);
            }
            else
            {
                pb.Maximum = value;
            }
        }

        // thread safe show exception message
        public static void ShowWarningFromException(this RichTextBox tb, Exception exception)
        {
            if (tb.InvokeRequired)
                tb.Invoke(new Action<RichTextBox, Exception>(ShowWarningFromException), tb, exception);
            else
                MessageBox.Show(tb, exception.Message, Resources.res_ExceptionHappened, MessageBoxButtons.OK,
                MessageBoxIcon.Warning);
        }
    }
}
