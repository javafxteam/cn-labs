﻿using System;
using System.Globalization;
using System.Linq;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Extensions
{
    public static class VectorExtensions
    {
        /// <summary>
        /// Given the input string in the form of an array sepparated by spaces, it builds the current vector from that string
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="inputSeparatedByLineBreaks">Possible input: '1\n2\n3'</param>
        /// <param name="id">Represents the id of this vector, to help identify if possible exceptions.</param>
        public static void BuildFromString(this Vector<double> vec, string inputSeparatedByLineBreaks, string id = "unknown")
        {
            var n = vec.Values.Capacity;
            var values = inputSeparatedByLineBreaks.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (values.Length != n)
                throw new Exception($"The number of rows is not {n}. Error occurred on: {id}.");

            for (var j = 0; j < values.Length; j++)
            {
                try
                {
                    vec[j] = double.Parse(values[j], NumberStyles.Any, CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    throw new Exception($"You must insert double numbers. Error occurred on: {id}.");
                }
            }
        }

        public static double GetNorm(this Vector<double> vector)
        {
            var sum = 0.0;
            vector.Values.ForEach(x => sum += Math.Pow(x, 2));
            return Math.Sqrt(sum);
        }

        public static double GetInfinityNorm(this Vector<double> vector)
        {
            return vector.Values.Select(Math.Abs).Max();
        }
    }
}
