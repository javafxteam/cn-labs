﻿using System;
using System.IO;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Extensions
{
    public static class LogicExtensions
    {
        /// <summary>
        /// The StreamReader goes N lines forward and returns the string.
        /// </summary>
        /// <param name="n">The number of lines to read.</param>
        /// <returns></returns>
        public static string ReadMaxNLines(this StreamReader reader, int n)
        {
            var output = "";
            for (var i = 0; i < n; i++)
            {
                output += reader.ReadLine() + "\n";
            }
            return output;
        }

        /// <summary>
        /// Returns Random double numbers, != 0
        /// </summary>
        /// <param name="random"></param>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static double GetRandomDouble(this Random random, double minimum, double maximum, bool allowZeros = false)
        {
            var val = 0.0;
            if (!allowZeros)
            {
                while (Math.Abs(val) < Utils.Precision)
                    val = random.NextDouble() * (maximum - minimum) + minimum;
            }
            else
            {
                val = random.NextDouble() * (maximum - minimum) + minimum;
            }
            return val;
        }
    }
}
