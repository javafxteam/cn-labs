﻿
using System;
using System.Collections.Generic;
using System.Linq;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class Vector<T>
        where T: new()
    {
        public List<T> Values { get; set; } = new List<T>();

        /// <summary>
        /// Returns the size of the vector.
        /// </summary>
        public int Count => Values.Count;


        public Vector(){ }

        public Vector(int capacity)
        {
            Values = new List<T>(capacity);
            for (var i = 0; i < capacity; i++)
            {
                Values.Add(new T());
            }
        }

        public Vector(Vector<T> copyVector)
        {
            Values = copyVector.Values.ToList();
        }
        
        public T this[int index]
        {
            get
            {
                if(index < 0 || index >= Values.Count)
                    throw new ArgumentException();
                return Values[index];
            }
            set
            {
                if (index < 0 || index >= Values.Capacity)
                    throw new ArgumentException();
                if (index < Values.Count)
                    Values[index] = value;
                else
                    Values.Insert(index, value);
            }
        }

        public static Vector<T> operator -(Vector<T> v1, Vector<T> v2)
        {
            var output = new Vector<T>(v1.Count);
            for (var i = 0; i < v1.Count; i++)
            {
                output[i] = (dynamic)v1[i] - (dynamic)v2[i];
            }
            return output;
        }

        public static double operator *(Vector<T> v1, Vector<T> v2)
        {
            var sum = 0.0;
            var index = 0;
            v1.Values.ForEach(x => sum += (dynamic)x * (dynamic)v2[index++]);
            return sum;
        }

        public static Vector<double> operator *(double multiply, Vector<T> v)
        {
            var x = v as Vector<double>;// attempt cast

            if (x == null)
                throw new Exception("Multiply only works on doubles.");

            var vector = new Vector<double>(v.Count)
            {
                Values = x.Values.Select(val => val * multiply).ToList()
            };
            return vector;
        }


        public static Vector<double> MakeRandomVector(int n)
        {
            var vector = new Vector<double>(n);
            var r = new Random();
            for (var i = 0; i < n; i++)
            {
                vector[i] = r.GetRandomDouble(-Utils.MinMaxGenerated, Utils.MinMaxGenerated);
            }
            return vector;
        }

        public static Vector<double> GetVectorMultipliedWithNorm(Vector<double> x)
        {
            return (1.0 / x.GetNorm()) * x;
        }

        public static Vector<double> MakeRandomVectorWithNorm1(int n)
        {
            var x = MakeRandomVector(n);
            return GetVectorMultipliedWithNorm(x);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Vector<T>))
                return false;

            var vec2 = obj as Vector<double>;
            var vec1 = this as Vector<double>;
            if (vec2 == null || vec1 == null || vec1.Count != vec2.Count)
                return false;
            
            for(var i = 0; i < vec1.Count; i++)
                if (Math.Abs(vec1[i] - vec2[i]) > Utils.Precision)
                    return false;
            return true;
        }

        protected bool Equals(Vector<T> other)
        {
            return Equals(Values, other.Values);
        }

        public static bool operator ==(Vector<T> v1, Vector<T> v2)
        {
            return Equals(v1, v2);
        }
        public static bool operator !=(Vector<T> v1, Vector<T> v2)
        {
            return !(v1 == v2);
        }

        public string ToFullString()
        {
            return string.Join("\n", Values.Select(x => x.ToString())) + $" [{Values.Count} elements].";
        }

        public override string ToString()
        {
            return string.Join(", ", Values.Take(5).Select(x => x.ToString())) + $" [{Values.Count} elements].";
        }
    }
}
