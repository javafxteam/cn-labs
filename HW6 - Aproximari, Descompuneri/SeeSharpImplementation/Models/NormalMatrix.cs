﻿using System;
using System.Globalization;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Factorization;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Logic;

namespace SeeSharpImplementation.Models
{
    public class NormalMatrix
    {
        private readonly Backgrounder _worker;//represents the worker that runs me. ref it to make him update
        public int N { get; set; }
        public int M { get; set; }
        public Matrix Matrix { get; set; }
        public Svd<double> LocalSvd { get; set; }
        public Matrix As { get; set; }

        public NormalMatrix(int m, int n, Backgrounder worker)
        {
            M = m;
            N = n;
            if (N == 0)
            {
                var r = new Random();
                N = r.Next(1, M);
            }
            Matrix = new DenseMatrix(M, N);
            _worker = worker;
        }

        public void ReadFromFile(string inputLines)
        {
            var lines = inputLines.Split(new[] { "\n\r", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);

            // start transforming
            _worker.AddLogMessage("Reading Matrix.");
            _worker.UpdateProgress(0);
            _worker.UpdateUpperBounds(M * N + 1);

            for (var i = 0; i < M; i++)
            {
                var columns = lines[i].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                for (var j = 0; j < N; j++)
                {
                    var value = double.Parse(columns[j], NumberStyles.Any, CultureInfo.InvariantCulture);
                    Matrix[i,j] = value;
                    _worker.UpdateProgress(i * N + j);
                }
            }
        }

        public void MakeMeRandom()
        {
            var r = new Random();
            for(var i = 0; i < M; i++)
                for (var j = 0; j < N; j++)
                    Matrix[i, j] = r.GetRandomDouble(-Utils.MinMaxGenerated, Utils.MinMaxGenerated, true);
        }


        public Svd<double> Svd()
        {
            LocalSvd = Matrix.Svd();
            return LocalSvd;
        }

        public double GetInfNormWithSvd()
        {
            return (Matrix - LocalSvd.U * LocalSvd.W * LocalSvd.VT).InfinityNorm();
        }

        public Matrix ComputeAs(int s)
        {
            if(s > LocalSvd.Rank)
                return null;

            As = new DenseMatrix(M, N);

            for (var i = 0; i < s; i++)
            {
                var sv = LocalSvd.W[i,i];
                var ui =  LocalSvd.U.Column(i);
                var vi = LocalSvd.VT.ConjugateTranspose().Column(i);
                ui = sv * ui;
                var mulMat = ui.OuterProduct(vi);
                As = (Matrix) (As + mulMat);
            }

            return As;
        }

        public double GetInfNormWithAs()
        {
            return (Matrix - As).InfinityNorm();
        }


        public string ToFullString()
        {
            return Utils.GetMatrixStringRepresentation(M, N, (i, j) => Matrix[i, j].ToString(CultureInfo.InvariantCulture));
        }

        public override string ToString()
        {
            var tempMat = new string[5,5];
            for(var x = 0; x < 5; x ++)
            for (var y = 0; y < 5; y++)
                tempMat[x, y] = x < M && y < N ? Matrix[x, y].ToString(CultureInfo.InvariantCulture) : ".";

            return $"Matrix ({M}x{N}):\n" + 
                Utils.GetMatrixStringRepresentation(7, 7, (i, j) =>
            {
                if (i < 5 && j < 5)
                    return tempMat[i, j];

                if (i <= 6 && j <= 6)
                    return ".";

                return "";
            });
        }
    }
}
