﻿
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Logic;
using System;
using System.Globalization;
using System.Linq;

namespace SeeSharpImplementation.Models
{
    public class SpecialMatrix
    {
        private Backgrounder _worker;//represents the worker that runs me. ref it to make him update
        private int[] _rowsStart;

        public int N { get; set; }

        public int Nn { get; set; }
        public Vector<double> Diagonal { get; set; }
        public Vector<Cell> Cells { get; set; }


        public SpecialMatrix(int n, Backgrounder worker)
        {
            _worker = worker;

            N = n;
			Diagonal = new Vector<double>(N);

			// init cells, with empty rows
            Cells = new Vector<Cell>();
            _rowsStart = new int[N + 1];
            for (var i = 1; i <= N + 1; i++)
            {
                Cells.Values.Add(new Cell(0, -i));
				_rowsStart[i - 1] = i - 1;// this means the row (i) starts at i - 1 for now
            }
        }

        public void ReadFromFile(string inputLines, bool diagonalCanHaveNulls = true)
        {
            var values = inputLines.Split(new[] { "\n\r", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
            Nn = values.Length;

            // start transforming
            _worker.AddLogMessage("Reading Matrix.");
            _worker.UpdateProgress(0);
            _worker.UpdateUpperBounds(Nn + 1);

            for (var i = 0; i < Nn; i++)
            {
                var data = values[i].Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                var value = double.Parse(data[0], NumberStyles.Any, CultureInfo.InvariantCulture);
                var row = int.Parse(data[1]);
                var column = int.Parse(data[2]);
                InsertValue(value, row, column);

                _worker.UpdateProgress(i + 1);
            }

            if (diagonalCanHaveNulls) return;
            //check if elements from diagonal missing
            var diffs = Diagonal.Values.Count(e => Math.Abs(e) < Utils.Precision);
            if (diffs != 0)
                throw new Exception($"All the elements from the diagonal should be != 0.\nElements == 0 (Epsilon-wise) found: {diffs}.");
        }

        /// <summary>
        /// Make a new matrix. Matricea are cel mult 10 elemente nenule pe fiecare linie (inspirat din T3)
        /// </summary>
        public void MakeMeRandomSymmetric()
        {
            var r = new Random();
            var rowsWithCount = new int[N];
            var rowsWithMax = new int[N];
            Nn = 0;


            for (var i = 0; i < N; i++)
            {
                rowsWithCount[i] = 0;
                rowsWithMax[i] = r.Next(1, Utils.MaxElementsPerLine);
            }

            // gen line by line
            _worker.UpdateUpperBounds(N);
            for (var i = 0; i < N; i++)
            {
                while(rowsWithCount[i] < rowsWithMax[i]) // while needed no. of elements not achieved
                {
                    var column = r.Next(0, N);
                    var value = r.GetRandomDouble(-Utils.MinMaxGenerated, Utils.MinMaxGenerated);

                    // add to row with column-index only if not already there
                    if (rowsWithCount[column] >= rowsWithMax[column]) continue;

                    InsertValue(value, i, column);
                    InsertValue(value, column, i);

                    rowsWithCount[i]++;
                    rowsWithCount[column]++;
                    Nn++;
                }
                _worker.UpdateProgress(i + 1);
            }
        }

        public bool AmISymmetric()
        {
            _worker.UpdateUpperBounds(N);
            for (var i = 0; i < N; i++)
            {
                var rowIStart = GetRowStart(i);
                var rowIEndCell = GetRowEndCell(i);
                var index = rowIStart + 1;

                do
                {
                    var currentICell = Cells.Values[index];
                    if (currentICell.Equals(rowIEndCell))
                        break;
                    
                    Cell currentJCell;
                    var iHas = false;
                    var rowJStart = GetRowStart(currentICell.Column);
                    var rowJEndCell = GetRowEndCell(currentICell.Column);
                    var indexJ = rowJStart + 1;

                    do
                    {
                        currentJCell = Cells.Values[indexJ];
                        if (currentJCell.Column == i)
                            iHas = true;
                        indexJ++;
                    }
                    while (!currentJCell.Equals(rowJEndCell) && !iHas);

                    if (!iHas)
                        return false;

                    index++;
                }
                while (true);
                _worker.UpdateProgress(i + 1);
            }

            return true;
        }


        /**
         * Multiplies this matrix with a vector.
         */
        public static Vector<double> operator *(SpecialMatrix matrix, Vector<double> vector)
        {
            var row = -1;
            var output = new Vector<double>(vector);

            foreach (var cell in matrix.Cells.Values)
            {
                var column = cell.Column;
                if (column < 0)
                {
                    ++row;
                    continue;
                }

                output[row] += cell.Value * vector[column];
            }
            return output;
        }

        public Characteristic GetCharacteristic()
        {
            var c = new Characteristic();

            var v = Vector<double>.MakeRandomVectorWithNorm1(N);
            double norm, val;
            var w = this * v;
            var k = 0;

            _worker.UpdateUpperBounds(5000);
            do
            {
                v = Vector<double>.GetVectorMultipliedWithNorm(w);
                w = this * v;
                val = w * v;
                norm = (w - val * v).GetNorm();
                k++;
                _worker.UpdateProgress(k % 5000);

            } while (norm > Utils.Precision && k < Utils.MaxNumberOfStepsForPowMethod);

            if(k > Utils.MaxNumberOfStepsForPowMethod)
                throw new Exception("Algorithm exceeded max number of steps. Cannot find characteristic.");

            c.Value = val;
            c.Vector = v;
            return c;
        }

        public int GetRowStart(int row)
        {
            //var startRowIdx = Cells.Values.IndexOf(new Cell(0, -(row + 1)));
            return _rowsStart[row];
        }

        public Cell GetRowEndCell(int row)
        {
            return new Cell(0, -(row + 2));
        }

        private void InsertValue(double value, int row, int column)
        {
            if (row == column)
            {
                // check for almost zero elements
                if (Math.Abs(value) < Utils.Precision)
                    throw new Exception($"Found element < epsilon on diagonal at ({row},{row}).");

                Diagonal[row] += value;
                return;
            }

            // IndexOf kills  me here 😧
            var startRowIdx = GetRowStart(row);

            var index = startRowIdx + 1;
            Cell currentCell;
            var endCell = GetRowEndCell(row);

            do
            {
                //var newCell = new Cell(value, column);//used when not caring about double entries
                // Cells.Values.Insert(index, newCell);

                currentCell = Cells.Values[index];
                // update
                if (currentCell.Column == column)
                {
                    currentCell.Value += value;
                    break;
                }

                // add only at the end of the row OR column-sorted
                if (currentCell.Column > column || currentCell.Equals(endCell))
                {
                    var newCell = new Cell(value, column);
                    Cells.Values.Insert(index, newCell);

                    for (var i = row + 1; i < _rowsStart.Length; i++)//update new beginnings
                        _rowsStart[i]++;

                    break;
                }
                index++;
            }
            while (!currentCell.Equals(endCell));
        }

        public override string ToString()
        {
            return $"N: {N}, " + $"NN + N: {Nn}\n" + "Diagonal: " + Diagonal + "\n" + "Values: " + Cells;
        }

        public string ToFullString()
        {
            return $"N: {N}, " + $"NN + N: {Nn}\n" + "Diagonal:\n" + Diagonal.ToFullString() + "\n\n" + "Values:\n" + Cells.ToFullString();
        }
    }
}
