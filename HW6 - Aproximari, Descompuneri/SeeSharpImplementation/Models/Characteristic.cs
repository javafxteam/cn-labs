﻿namespace SeeSharpImplementation.Models
{
    public class Characteristic
    {
        public Vector<double> Vector { get; set; }
        public double Value { get; set; }

        public override string ToString()
        {
            return $"Characteristic:\nValue: {Value};\nVector: {Vector}]";
        }
    }
}
