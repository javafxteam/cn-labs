﻿using System.Globalization;
using System.IO;
using MathNet.Numerics.LinearAlgebra.Factorization;
using SeeSharpImplementation.Extensions;
using SeeSharpImplementation.Models;

namespace SeeSharpImplementation.Logic
{
    public class Controller
    {
        private Backgrounder _worker;//represents the worker that runs me. ref it to make him update

        public int N { get; set; }
        public int P { get; set; }
        public int S { get; set; }
        public bool IsPeqToN { get; set; }
        public SpecialMatrix A { get; set; }
        public NormalMatrix M { get; set; }
        public Vector<double> B { get; set; }
        public bool AIsSymmetric { get; set; }

        public Characteristic C { get; set; }
        public bool IsPowSolutionOk { get; set; }
        private Vector<double> _vec1;
        private Vector<double> _vec2;

        private Svd<double> Svd;
        

        public Controller(Backgrounder worker)
        {
            _worker = worker;
        }

        public void SetExecuter(Backgrounder worker)
        {
            _worker = worker;
        }

        public void ReadMatrixFromFile(string filePath)
        {
            if(IsPeqToN)
                ReadRareMatrixFromFile(filePath, false);
            else
                ReadClassicMatrixFromFile(filePath);
        }

        public void ReadRareMatrixFromFile(string filePath, bool hasB = true)
        {
            using (var reader = new StreamReader(filePath))
            {
                // read N
                _worker.AddLogMessage("Reading data from file...");
                _worker.UpdateUpperBounds(2);

                N = int.Parse(reader.ReadLine());
                reader.ReadLine();
                _worker.UpdateProgress(0);

                // read B
                if (hasB)
                {
                    var bString = reader.ReadMaxNLines(N);
                    B = new Vector<double>(N);
                    B.BuildFromString(bString);
                    _worker.UpdateProgress(1);
                }

                // read matrix
                var mString = reader.ReadToEnd();
                _worker.UpdateProgress(2);

                A = new SpecialMatrix(N, _worker);
                A.ReadFromFile(mString);
            }
        }

        public void ReadClassicMatrixFromFile(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            {
                // read N
                _worker.AddLogMessage("Reading data from file...");
                _worker.UpdateUpperBounds(2);

                P = int.Parse(reader.ReadLine());
                N = int.Parse(reader.ReadLine());
                reader.ReadLine();
                _worker.UpdateProgress(0);
                
                // read matrix
                var mString = reader.ReadToEnd();
                _worker.UpdateProgress(2);

                M = new NormalMatrix(P, N, _worker);
                M.ReadFromFile(mString);
            }
        }

        public void MakeRandomMatrix()
        {
            if (IsPeqToN)
            {
                A = new SpecialMatrix(P, _worker);
                A.MakeMeRandomSymmetric();
            }
            else
            {
                M = new NormalMatrix(P, N, _worker);
                M.MakeMeRandom();
            }
        }

        public void TestSymmetry()
        {
            AIsSymmetric = A.AmISymmetric();
        }

        public string GetSymmetricMatrixString()
        {
            return AIsSymmetric ? $"The matrix {A.N}x{A.N} is symmetric." : $"The matrix {A.N}x{A.N} is NOT symmetric.";
        }

        public void RunPowMethod()
        {
            C = A.GetCharacteristic();
            _vec1 = A * C.Vector;
            _vec2 = C.Value * C.Vector;
            IsPowSolutionOk = _vec1 == _vec2;
        }

        public string GetPowMethodResultMessage()
        {
            return C +
                   "\nDiff vector: " + (_vec1 - _vec2) +
                   "\n...Testing if A * u == Value * u...\n" + (IsPowSolutionOk
                ? "The Solution is Ok."
                : "The Solution is NOT Ok.");

        }

        public void RunSvdMethod()
        {
            Svd = M.Svd();
        }

        public string GetSvdData()
        {
            var singleValues = Svd.S.ToString();
            var rank = Svd.Rank.ToString();
            var condNumber = Svd.ConditionNumber.ToString(CultureInfo.InvariantCulture);
            var norm = M.GetInfNormWithSvd().ToString(CultureInfo.InvariantCulture);
            var As = M.ComputeAs(S);
            return "Single values:\n" + singleValues +
                   "\nRank: " + rank +
                   "\nConditional Number: " + condNumber +
                   "\n||A - USV||..: " + norm +
                   (As != null ? $"\nThe As Matrix (s = {S}):\n" + As +
                   "\n||A - As||..: " + M.GetInfNormWithAs() : $"\nS is bigger than rank(A) (S is {S}, rank(A) is {rank}). Cannot compute.");
        }

        public string GetMatrixString()
        {
            return IsPeqToN ? A.ToString() : M.ToString();
        }

        public string GetMatrixFullString()
        {
            return IsPeqToN ? A.ToFullString() : M.ToFullString();
        }
    }
}
