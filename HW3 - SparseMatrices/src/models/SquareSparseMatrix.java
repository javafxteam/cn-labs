package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created on 25.03.2017.
 * @author Cezara C.
 * @author Marian F.
 * A <code>SquareSparseMatrix</code> uses an economical scheme for storing a matrix.
 * This scheme uses 3 arrays:
 * <ul>
 *     <li>one for the diagonal elements (n sized)</li>
 *     <li>one for the other non-zero elements</li>
 *     <li>one for column indexes of the elements in the previous vector</li>
 * </ul>
 * The last two vectors are compressed into one using <code>MatrixEntry</code>.
 * @see MatrixEntry
 */
public class SquareSparseMatrix {

    private List<Double> diagonal = new ArrayList<>();

    /**
     * Contains the non-diagonal non-zero matrix elements in ascending order
     * of the column index of the <code>MatrixEntry</code>. The starting
     * of a new row is indicated by a <code>MatrixEntry</code> with <code>value</code>
     * set to 0 and <code>column</code> set to minus the index of the row (counting from 1).
     * The last element is always a <code>MatrixEntry</code> with <code>value</code> set to 0
     * and <code>column</code> set to minus <code>size</code>.
     */
    private List<MatrixEntry> entries = new ArrayList<>();
    private int size;

    /**
     * Adds a matrix <code>value</code>in its corresponding position in <code>entries</code> list
     * by identifying its <code>row</code> and then its exact place based on the <code>column</code>
     * index (which is assumed to be and should remain in ascending order).
     * @param value the value of the entry
     * @param row the row that this entry belong to
     * @param column the column index of the value in the indicated row
     */
    public void insertEntry(double value, int row, int column) {
        int startRowIdx = entries.indexOf(new MatrixEntry(0, -(row + 1)));
        int endRowIdx = entries.indexOf(new MatrixEntry(0, -(row + 2)));

        int idx = startRowIdx + 1;

        while(idx <= endRowIdx)
        {
            MatrixEntry currentEntry = entries.get(idx);
            if(currentEntry.getColumn() == column)
            {
                currentEntry.add(value);
                break;
            }

            if(currentEntry.getColumn() > column || idx == endRowIdx)
            {
                MatrixEntry entry = new MatrixEntry(value, column);
                entries.add(idx, entry);
                break;
            }
            ++idx;
        }
    }

    /**
     * Adds the parameter matrix to this one by:
     * <ul>
     *     <li>adding the diagonal lists element by element</li>
     *     <li>using the <code>insertEntry</code> method for the rest of elements</li>
     * </ul>
     * @param matrix the matrix to add
     * @return a new <code>SquareSparseMatrix</code> representing the sum between <code>this</code>
     * and the matrix given as parameter
     * @throws IllegalArgumentException if the sizes are not equal
     * @see SquareSparseMatrix#insertEntry(double, int, int)
     */
    public SquareSparseMatrix add(SquareSparseMatrix matrix) {
        if(size != matrix.size)
            throw new IllegalArgumentException("Sizes of matrices must be equal.");

        SquareSparseMatrix result = new SquareSparseMatrix();
        result.setSize(size);

        for(int i = 0; i < size; ++i)
            result.getDiagonal().add(diagonal.get(i) + matrix.diagonal.get(i));

        for (MatrixEntry entry : entries) {
            result.getEntries().add(entry.copy());
        }

        int row = -1;
        for (MatrixEntry entry : matrix.getEntries()) {
            if(entry.getColumn() < 0)
                ++row;
            else
                result.insertEntry(entry.getValue(), row, entry.getColumn());
        }

        return result;
    }

    /**
     * * Adds the parameter matrix to this one by:
     * <ul>
     *     <li>adding the diagonal lists element by element</li>
     *     <li>using a merge-sort like approach for adding the rest of the elements (better complexity)</li>
     * </ul>
     * @param matrix the matrix to add
     * @return a new <code>SquareSparseMatrix</code> representing the sum between <code>this</code>
     * and the parameter matrix
     * @throws IllegalArgumentException if the sizes are not equal
     */
    public SquareSparseMatrix add2(SquareSparseMatrix matrix) {

        if(size != matrix.size)
            throw new IllegalArgumentException("Sizes of matrices must be equal.");

        SquareSparseMatrix result = new SquareSparseMatrix();
        result.setSize(size);

        for(int i = 0; i < size; ++i)
            result.getDiagonal().add(diagonal.get(i) + matrix.diagonal.get(i));

        int thisIdx = 0, thatIdx = 0;
        int row = -1;
        int thisEntryCol = -1, thatEntryCol = -1;
        int end = -(size + 1);
        while(true)
        {
            if(thisEntryCol == end && thatEntryCol == end)
                break;

            MatrixEntry thisEntry = entries.get(thisIdx), thatEntry = matrix.getEntries().get(thatIdx);

            if(thisEntry.equals(thatEntry) && thisEntry.getValue() == 0)
            {
                result.getEntries().add(new MatrixEntry(0, row));
                --row;
                ++thisIdx;
                ++thatIdx;
                continue;
            }

            thisEntryCol = thisEntry.getColumn();
            thatEntryCol = thatEntry.getColumn();

            if(thisEntryCol == thatEntryCol)
            {
                result.getEntries().add(thisEntry.add(thatEntry));
                ++thisIdx;
                ++thatIdx;
                continue;
            }

            if(thisEntryCol < thatEntryCol)
            {
                //got to the end of the row in first matrix so add the rest of the elements (same row) of second matrix
                if(thisEntryCol < 0)
                {
                    while (thatEntryCol > 0)
                    {
                        result.getEntries().add(thatEntry.copy());
                        ++thatIdx;
                        thatEntry = matrix.getEntries().get(thatIdx);
                        thatEntryCol = thatEntry.getColumn();
                    }
                }
                else
                {
                    result.getEntries().add(thisEntry.copy());
                    ++thisIdx;
                }
            }
            else
            {
                //got to the end of the row in second matrix so add the rest of the elements (same row) of first matrix
                if(thatEntryCol < 0)
                {
                    while (thisEntryCol > 0)
                    {
                        result.getEntries().add(thisEntry.copy());
                        ++thisIdx;
                        thisEntry = entries.get(thisIdx);
                        thisEntryCol = thisEntry.getColumn();
                    }
                }
                else
                {
                    result.getEntries().add(thatEntry.copy());
                    ++thatIdx;
                }
            }

        }

        result.getEntries().add(new MatrixEntry(0, end));
        return result;
    }

    /**
     * Multiplies this with the parameter matrix by:
     * <ul>
     *
     * </ul>
     * @param matrix the matrix to multiply with
     * @return a new <code>SquareSparseMatrix</code> representing the product between <code>this</code>
     * and the parameter matrix
     * @throws IllegalArgumentException
     */
    public SquareSparseMatrix multiply(SquareSparseMatrix matrix) {
        if(size != matrix.size)
            throw new IllegalArgumentException("Sizes of matrices must be equal.");

        SquareSparseMatrix result = new SquareSparseMatrix();
        result.setSize(size);

        int thisRow = 0, thatRow = 0;
        double[] result_row = new double[size];
        int end = (-size) - 1;

        result.entries.add(new MatrixEntry(0, -1));
        for (MatrixEntry thisEntry : this.entries) {

            int thisEntryCol = thisEntry.getColumn();

            if (thisEntryCol < 0) {
                thisRow = (-thisEntryCol) - 1;

                if (thisEntryCol != -1) {
                    for (int i = 0; i < size; ++i) {
                        double value = result_row[i];
                        if (i == thisRow - 1)
                            result.diagonal.add(value);
                        else if (value != 0)
                            result.entries.add(new MatrixEntry(value, i));

                        result_row[i] = 0;
                    }
                    result.entries.add(thisEntry.copy());
                }

                if (thisEntryCol == end)
                    break;

                result_row[thisRow] += this.diagonal.get(thisRow) * matrix.diagonal.get(thisRow);
            }
            else
                result_row[thisEntryCol] += thisEntry.getValue() * matrix.diagonal.get(thisEntryCol);

            for (MatrixEntry thatEntry : matrix.entries) {

                int thatEntryCol = thatEntry.getColumn();

                if(thatEntryCol < 0)
                {
                    thatRow = (-thatEntryCol) - 1;
                    continue;
                }

                if(thisEntryCol < 0 && thisRow == thatRow)
                {
                    double diag_value = this.diagonal.get(thisRow);
                    result_row[thatEntryCol] += diag_value * thatEntry.getValue();
                    continue;
                }

                if(thisEntryCol == thatRow)
                    result_row[thatEntryCol] += thisEntry.getValue() * thatEntry.getValue();
            }
        }
        return result;
    }


    /**
     * Multiplies this matrix with a vector of the same size with
     * values descending by 1 from <code>size</code> to 1.
     * @return the result vector
     * @throws IllegalArgumentException
     */
    public List<Double> multiply() {
        Double[] temp = new Double[size];
        for(int i = 0; i < size; ++i)
            temp[i] = diagonal.get(i) * (size - i);

        int row = -1;

        for (MatrixEntry entry : entries) {

            int column = entry.getColumn();
            if(column < 0)
            {
                ++row;
                continue;
            }

            temp[row] += entry.getValue() * (size - column);
        }

        return Arrays.asList(temp);
    }

    /**
     * Tests if a <code>SquareSparseMatrix</code> is sparse by verifying that
     * there are a maximum number of non-zero elements per line.
     * @param max the maximum number of non-zero elements per line
     * @return <coce>true</coce> if this matrix is considered a sparse one,
     * <code>false</code> otherwise
     */
    public boolean isSparse(int max) {

        if(max < 0)
            throw new IllegalArgumentException("Only positive integers accepted.");

        int counter = 0;

        int row = 0;
        for(int i = 1; i < size; ++i)
        {
            MatrixEntry entry = entries.get(i);

            if(entry.getColumn() < 0)
            {
                counter = 0;
                if(diagonal.get(row) > 0)
                    ++counter;
                ++row;
            }
            else
            {
                ++counter;
                if(counter > max)
                    return false;
            }
        }
        return true;
    }

    public List<Double> getDiagonal() {
        return diagonal;
    }

    public List<MatrixEntry> getEntries() {
        return entries;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if(size <= 0)
            throw new IllegalArgumentException("Size of matrix must be positive.");
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SquareSparseMatrix matrix = (SquareSparseMatrix) o;

        if (size != matrix.size) return false;
        if (!diagonal.equals(matrix.diagonal)) return false;

        return entries.equals(matrix.entries);
    }

    @Override
    public int hashCode() {
        int result = diagonal.hashCode();
        result = 31 * result + entries.hashCode();
        result = 31 * result + size;
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Size: ").append(size).append("\n");

        sb.append("Diagonal: ").append("\n");
        for (Double value : diagonal)
            sb.append(value).append("\n");

        sb.append("Entries:\n");
        for (MatrixEntry entry : entries)
            sb.append(entry).append("\n");

        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
