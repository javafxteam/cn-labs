package models;

/**
 * Created on 25.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */

import javafx.util.Pair;

/**
 * A <code>MatrixEntry</code> stores a matrix non-zero value along with
 * its column index.
 */
public class MatrixEntry {

    private double value;
    private int column;

    public MatrixEntry(double value, int column) {
        this.setValue(value);
        this.setColumn(column);
    }

    public void add(double value) {
        this.value += value;
    }

    public MatrixEntry add(MatrixEntry other) {
        if(column != other.column)
            throw new IllegalArgumentException("Cannot add entries that have different column indices.");

        return new MatrixEntry(value + other.getValue(), column);
    }

    @Override
    public boolean equals(Object other) {

        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        MatrixEntry that = (MatrixEntry) other;

        if (Double.compare(this.value, that.value) != 0) return false;
        return column == that.column;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(value);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + column;
        return result;
    }

    @Override
    public String toString() {

        return String.format("(%f, %d)", value, column);
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public MatrixEntry copy() {
        return new MatrixEntry(value, column);
    }
}
