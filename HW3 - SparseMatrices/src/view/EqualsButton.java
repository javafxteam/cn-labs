package view;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import view.data.DataBox;
import view.data.DataEntry;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class EqualsButton extends Button {

    private DataBox matrixDataBox;
    private DataBox vectorDataBox;

    public EqualsButton(DataBox matrixDataBox, DataBox vectorDataBox) {

        super();
        this.matrixDataBox = matrixDataBox;
        this.vectorDataBox = vectorDataBox;

        setText("Are Equal");
        visibleProperty().bind(Bindings.size(matrixDataBox.getSelectionModel().getSelectedItems()).isEqualTo(2)
                .or(Bindings.size(vectorDataBox.getSelectionModel().getSelectedItems()).isEqualTo(2)));

        setOnMouseClicked(event -> {

            Logger logger = Logger.getInstance();
            DataBox dataBox = matrixDataBox.getSelectionModel().getSelectedItems().size() == 2 ? matrixDataBox : vectorDataBox;

            ObservableList selected = dataBox.getSelectionModel().getSelectedItems();
            DataEntry first = (DataEntry) selected.get(0);
            DataEntry second = (DataEntry) selected.get(1);
            boolean result = first.equals(second);

            String message = String.format("%s %s is%s equal to %s %s.", first.type(), first.getName(),
                                                result ? "" : " not", second.type(), second.getName());
            logger.append("INFO", message);

            dataBox.getSelectionModel().clearSelection();
        });
    }
}
