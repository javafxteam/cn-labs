package view;

import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import view.data.DataBox;
import view.data.MatrixViewEntry;

/**
 * Created on 28.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class SparseButtonMenu extends HBox {

    private Window window;
    private DataBox dataBox;
    private Button button;
    private TextField max_field;

    public SparseButtonMenu(Window window, DataBox dataBox) {

        super();
        this.window = window;
        this.dataBox = dataBox;
        this.button = new Button("Is Sparse");

        this.max_field = new TextField();
        max_field.maxWidthProperty().bind(this.maxWidthProperty().multiply(0.4));
        max_field.getStyleClass().add("data-box");

        visibleProperty().bind(Bindings.size(dataBox.getSelectionModel().getSelectedItems()).isEqualTo(1));
        maxWidthProperty().bind(window.getScene().widthProperty().multiply(0.18));
        getStyleClass().add("sparse-menu");
        getChildren().addAll(button, max_field);

        button.setOnMouseClicked(event -> {

            Logger logger = Logger.getInstance();
            try
            {
                int max = Integer.parseInt(max_field.getText());

                MatrixViewEntry entry = (MatrixViewEntry) dataBox.getSelectionModel().getSelectedItem();

                long start = System.nanoTime();
                boolean result = entry.getData().isSparse(max);
                long end = System.nanoTime();
                double seconds = (end - start) / 1000000000d;

                String message = String.format("Matrix %s is%s sparse. Tested with %d non-zero values/row. Operation took: %f seconds."
                        , entry.getName(), result ? "" : " not", max, seconds);
                logger.append("INFO", message);
            }
            catch(Exception e)
            {
                if(e instanceof NumberFormatException)
                    logger.append("ERROR", "Please enter an integer value. ◉_◉");
                else
                {
                    logger.append("ERROR", e.getMessage());
                }
            }
        });
    }
}
