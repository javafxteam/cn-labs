package view;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import models.SquareSparseMatrix;
import view.data.DataBox;
import view.data.MatrixViewEntry;

/**
 * Created 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Utils {

    public interface MatrixOperationCaller {
        SquareSparseMatrix applyTo(SquareSparseMatrix first, SquareSparseMatrix second);
    }

    public static VBox getVBoxContainer() {
        VBox box = new VBox();
        box.getStyleClass().add("v-box");
        return box;
    }

    public static Text getTitleText(String title) {
        Text text = new Text(title);
        text.getStyleClass().add("data-box-title");
        return text;
    }

    public static void onMatrixOperationButtonClicked(Button button, DataBox matrixDataBox, MatrixOperationCaller operation)
    {
        button.setOnMouseClicked(event -> {
            ObservableList<MatrixViewEntry> matrices =  matrixDataBox.getSelectionModel().getSelectedItems();
            MatrixViewEntry first = matrices.get(0);
            MatrixViewEntry second = matrices.get(1);

            MatrixViewEntry entry = new MatrixViewEntry(null);

            long start = System.nanoTime();
            SquareSparseMatrix result = operation.applyTo(first.getData(), second.getData());
            long end = System.nanoTime();
            double seconds = (end - start) / 1000000000d;

            entry.setData(result);

            matrixDataBox.getItems().add(entry);

            String message = String.format("%s: %s to %s. Operation took: %f.", button.getText(), first.getName(), second.getName(), seconds);
            Logger.getInstance().append("INFO", message);

            matrixDataBox.getSelectionModel().clearSelection();
        });
    }
}
