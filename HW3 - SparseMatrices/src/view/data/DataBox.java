package view.data;


import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

import java.util.Objects;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class DataBox extends ListView {

    public DataBox() {

        super();

        getStyleClass().addAll("data-box");

        //mouse click + CTRL adds to selection or subtracts from selection if item already focused
        getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    public void onFocusClearSelectionFrom(DataBox other) {

        setOnMouseClicked(event -> {

            other.getSelectionModel().clearSelection();
        });
    }
}
