package view.data;

import java.io.File;
import java.util.List;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class VectorViewEntry extends DataEntry<List<Double>> {

    public VectorViewEntry(File file) {
        super(file);
    }

    @Override
    public String type() {
        return "vector";
    }
}
