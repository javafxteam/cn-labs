package view.data;

import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import models.SquareSparseMatrix;
import view.InputNameDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public abstract class DataEntry<T> extends HBox {

    private File file;
    private T data;
    private String name;

    public DataEntry(File file) {

        super();

        getStyleClass().add("data-entry");

        this.file = file;
        addImage();

        if(file != null)
        {
            this.file = file;
            add(String.format("File: %s\t", file.getName()));

            String name = file.getName().split("\\.txt")[0];
            this.name = name;
            add(String.format("Name: %s\t", name));
        }
        else
        {
            add("File: -\t");

            while(name == null || name.isEmpty())
            {
                InputNameDialog dialog = new InputNameDialog(this);

                Optional<String> name = dialog.showAndWait();
                if(name.isPresent())
                    this.name = name.get();
            }

            add(String.format("Name: %s\t", name));
        }
    }

    public void setData(T data)
    {
        this.data = data;
    }

    public T getData() { return this.data; }

    public String getName() { return this.name; }

    public abstract String type();

    public boolean equals(DataEntry<T> other)
    {
        return data.equals(other.data);
    }

    private void addImage()
    {
        ImageView imageView = new ImageView();
        Image image = new Image("view/matrix.png");
        imageView.setImage(image);

        imageView.setFitHeight(40);
        imageView.setFitWidth(40);

        getChildren().add(imageView);
    }

    private void add(String message)
    {
        Text text = new Text(message);
        text.setFont(Font.font("Helvetica", FontWeight.BOLD, 14));

        getChildren().add(text);
    }
}
