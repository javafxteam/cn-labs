package view.data;

import models.SquareSparseMatrix;
import view.data.DataEntry;

import java.io.File;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class MatrixViewEntry extends DataEntry<SquareSparseMatrix> {

    public MatrixViewEntry(File file)
    {
        super(file);
    }

    @Override
    public String type() {
        return "matrix";
    }
}
