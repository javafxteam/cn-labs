package view;

import controller.LoadButtonController;
import javafx.beans.binding.Bindings;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import models.SquareSparseMatrix;
import view.data.DataBox;
import view.data.MatrixViewEntry;
import view.data.VectorViewEntry;

import java.util.List;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Window extends Stage {

    public Window() {

        super();

        VBox up = Utils.getVBoxContainer();
        ScrollPane scrollPane = new ScrollPane();
        Logger logger = Logger.getInstance();
        logger.getChildren().addListener((ListChangeListener<Node>) ((change) -> {
            logger.layout();
            scrollPane.layout();
            scrollPane.setVvalue(1.0f);
        }));

        scrollPane.setContent(logger);
        scrollPane.minHeightProperty().bind(up.heightProperty().multiply(0.8));
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.getStyleClass().addAll("data-box");
        up.getChildren().add(scrollPane);

        //left
        VBox left = Utils.getVBoxContainer();
        DataBox matrixDataBox = new DataBox();
        DataBox vectorDataBox = new DataBox();
        matrixDataBox.onFocusClearSelectionFrom(vectorDataBox);
        vectorDataBox.onFocusClearSelectionFrom(matrixDataBox);
        //buttons
        ProgressIndicator progress = new ProgressIndicator(.0);
        progress.getStyleClass().add("data-box");
        progress.setVisible(false);

        Button load_button = new Button("Load");
        load_button.setOnMouseClicked(new LoadButtonController(this, progress, matrixDataBox, vectorDataBox));

        Button add_button = new Button("Add");
        add_button.visibleProperty().bind(Bindings.size(matrixDataBox.getSelectionModel().getSelectedItems()).isEqualTo(2));
        Utils.onMatrixOperationButtonClicked(add_button, matrixDataBox, SquareSparseMatrix::add2);

        Button multiply_button = new Button("Multiply");
        multiply_button.visibleProperty().bind(Bindings.size(matrixDataBox.getSelectionModel().getSelectedItems()).isEqualTo(2));
        Utils.onMatrixOperationButtonClicked(multiply_button, matrixDataBox, SquareSparseMatrix::multiply);

        Button equals_button = new EqualsButton(matrixDataBox, vectorDataBox);

        Button vectorize_button = new Button("Vectorize");
        vectorize_button.visibleProperty().bind(Bindings.size(matrixDataBox.getSelectionModel().getSelectedItems()).isEqualTo(1));
        vectorize_button.setOnMouseClicked(event -> {

            MatrixViewEntry entry = (MatrixViewEntry) matrixDataBox.getSelectionModel().getSelectedItem();
            long start = System.nanoTime();
            List<Double> result_vector = entry.getData().multiply();
            long end = System.nanoTime();

            double seconds = (end - start) / 1000000000d;

            VectorViewEntry result_entry = new VectorViewEntry(null);
            result_entry.setData(result_vector);

            vectorDataBox.getItems().add(result_entry);

            String message = String.format("Multiplied %s with [n,..., 2, 1] vector. Operation took: %f.", entry.getName(), seconds);
            Logger.getInstance().append("INFO", message);

            matrixDataBox.getSelectionModel().clearSelection();
        });

        //center area
        HBox center = new HBox();
        VBox matrixBox = Utils.getVBoxContainer();
        Text dataBoxTitle = Utils.getTitleText("Matrices");
        matrixBox.getChildren().addAll(dataBoxTitle, matrixDataBox);

        VBox vectorBox = Utils.getVBoxContainer();
        dataBoxTitle = Utils.getTitleText("Vectors");
        vectorBox.getChildren().addAll(dataBoxTitle, vectorDataBox);
        center.getChildren().addAll(matrixBox, vectorBox);

        BorderPane pane = new BorderPane();
        pane.setTop(up);
        pane.setLeft(left);
        pane.setCenter(center);

        up.maxHeightProperty().bind(pane.heightProperty().multiply(0.2));
        up.minHeightProperty().bind(pane.heightProperty().multiply(0.2));
        left.minWidthProperty().bind(pane.widthProperty().multiply(0.2));
        matrixBox.minWidthProperty().bind(pane.widthProperty().multiply(0.4));
        vectorBox.minWidthProperty().bind(pane.widthProperty().multiply(0.4));

        progress.maxWidthProperty().bind(left.minWidthProperty());
        load_button.minWidthProperty().bind(pane.widthProperty().multiply(0.16));

        Scene scene = new Scene(pane);
        setScene(scene);

        SparseButtonMenu sparse_menu = new SparseButtonMenu(this, matrixDataBox);
        left.getChildren().addAll(progress, load_button, vectorize_button, sparse_menu, equals_button, add_button, multiply_button);

        setup();
    }

    private void setup() {

        this.setTitle("Sparse Matrices Operations");

        Image icon = new Image("view/enter_the_matrix.png");
        this.getIcons().add(icon);

        setHeight(500);
        setWidth(900);

        setMinHeight(500);
        setMinWidth(700);

        getScene().getStylesheets().add("view/stylesheet.css");
    }
}
