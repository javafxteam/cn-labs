package view;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class Logger extends TextFlow {

    private static Logger instance;

    private Logger() {
        super();
        reset();
    }

    public static Logger getInstance() {

        if(instance == null)
            instance = new Logger();

        return instance;
    }

    private void reset()
    {
        getChildren().clear();
        append("INFO", "Read data and perform operations.");
    }

    public void append(String level, String message) {

        Text level_text = new Text(String.format("[%s] ", level));

        level_text.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        switch (level)
        {
            case "INFO":
                level_text.setFill(Color.GREEN);
                break;
            case "ERROR":
                level_text.setFill(Color.RED);
                break;
        }

        Text message_text = new Text(String.format("%s\n", message));
        message_text.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));

        getChildren().addAll(level_text, message_text);
    }
}
