package view;

import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import view.data.DataEntry;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class InputNameDialog extends TextInputDialog {

    private DataEntry entry;

    public InputNameDialog(DataEntry entry)
    {
        super();
        this.entry = entry;

        this.setTitle(String.format("Name your %s", entry.type()));
        this.setHeaderText(String.format("Give a name to your new %s to be able to identify it later. (ᵔᴥᵔ)", entry.type()));
        this.setContentText("Name:");

        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("view/enter_the_matrix.png"));
    }
}
