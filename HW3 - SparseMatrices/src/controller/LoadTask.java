package controller;

import javafx.concurrent.Task;
import models.MatrixEntry;
import models.SquareSparseMatrix;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A <code>Task</code> that reads a <code>SquareSparseMatrix</code> and its corresponding
 * result vector when multiplying her with the [n,...,2,1] vector and returns the
 * time this operation took upon completion.
 * Created on 28.03.2017.
 * @author Cezara C.
 * @author Marian F.
 * @see SquareSparseMatrix
 */
public class LoadTask extends Task<Double> {

    private String filename;
    private List<Double> vector;
    private SquareSparseMatrix matrix;

    public LoadTask(String filename) {

        this.filename = filename;
    }

    @Override
    protected Double call() throws Exception {

        vector = new ArrayList<>();
        matrix = new SquareSparseMatrix();

        long start = System.nanoTime();
        Path path = Paths.get(filename);
        try (Stream<String> stream = Files.lines(path)) {

            List<String> lines = stream.filter(sentence -> !sentence.equals("")).collect(Collectors.toList());
            int total = lines.size();

            int n = Integer.parseInt(lines.get(0));
            matrix.setSize(n);

            int line_idx = 1;
            for(; line_idx <= n; ++line_idx) {
                vector.add(Double.parseDouble(lines.get(line_idx)));
                update(line_idx, total);
            }

            //initialize entries in matrix based on the size
            for(int i = 1; i <= n + 1; ++i)
                matrix.getEntries().add(new MatrixEntry(0, -i));

            for(int i = 0; i < n; ++i)
                matrix.getDiagonal().add(0d);

            for(; line_idx < total; ++line_idx)
            {
                String[] parts = lines.get(line_idx).split(", ");
                double value = Double.parseDouble(parts[0]);
                int row = Integer.parseInt(parts[1]);
                int column = Integer.parseInt(parts[2]);

                if(row != column)
                    matrix.insertEntry(value, row, column);
                else
                {
                    double current = matrix.getDiagonal().get(row);
                    matrix.getDiagonal().set(row, current + value);
                }

                update(line_idx, total);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return (System.nanoTime() - start) / 1000000000d;
    }

    private void update(int line_idx, int total)
    {
        int div = total / 100;
        if(line_idx % div == 0)
            updateProgress(line_idx, total);
    }

    public List<Double> getVector() {
        return vector;
    }

    public SquareSparseMatrix getMatrix() {
        return matrix;
    }
}
