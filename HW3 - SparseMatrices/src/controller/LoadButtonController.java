package controller;

import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import view.Logger;
import view.Window;
import view.data.DataBox;
import view.data.MatrixViewEntry;
import view.data.VectorViewEntry;

import java.io.File;
import java.nio.file.Paths;

/**
 * An <code>EventHandler</code> for the <code>MouseEvent</code> to be added to
 * components in order to open files for reading data.
 * It starts a new <code>LoadTask</code> each time it is invoked.
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 * @see LoadTask
 */

public class LoadButtonController implements EventHandler<MouseEvent> {

    private Window window;
    private DataBox matrixDataBox;
    private DataBox vectorDataBox;
    private ProgressIndicator progress;

    public LoadButtonController(Window window, ProgressIndicator progress, DataBox matrixDataBox, DataBox vectorDataBox) {
        super();
        this.window = window;
        this.progress = progress;
        this.matrixDataBox = matrixDataBox;
        this.vectorDataBox = vectorDataBox;
    }

    @Override
    public void handle(MouseEvent event) {

        Logger logger = Logger.getInstance();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load New Data");
        fileChooser.setInitialDirectory(new File(Paths.get("./resources").toAbsolutePath().normalize().toString()));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TXT file (*.txt)", "*.txt"));
        File file = fileChooser.showOpenDialog(window);

        if(file != null)
        {
            progress.setVisible(true);

            final LoadTask task = new LoadTask(file.getAbsolutePath());
            progress.progressProperty().bind(task.progressProperty());
            task.stateProperty().addListener((obsStateValue, oldValue, newValue) -> {

                if(newValue == Worker.State.SUCCEEDED) {

                    MatrixViewEntry matrixViewEntry = new MatrixViewEntry(file);
                    matrixViewEntry.setData(task.getMatrix());
                    matrixDataBox.getItems().add(matrixViewEntry);

                    VectorViewEntry vectorViewEntry = new VectorViewEntry(file);
                    vectorViewEntry.setData(task.getVector());
                    vectorDataBox.getItems().add(vectorViewEntry);

                    logger.append("INFO", String.format("Loaded file: %s. Loading time: %f seconds", file.getName(), task.getValue()));

                    progress.progressProperty().unbind(); //so you can set it without throwing an exception
                    progress.setProgress(0.0);
                    progress.setVisible(false);
                }
            });

            (new Thread(task)).start();
        }
        else
        {
            logger.append("INFO", "Canceled loading data.");
        }
    }
}
