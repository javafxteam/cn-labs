package controller;

import javafx.application.Application;
import javafx.stage.Stage;
import view.Window;

/**
 * Created on 27.03.2017.
 * @author Cezara C.
 * @author Marian F.
 */
public class App extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Window window = new Window();
        window.show();
    }
}
